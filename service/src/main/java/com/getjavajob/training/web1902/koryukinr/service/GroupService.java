package com.getjavajob.training.web1902.koryukinr.service;

import com.getjavajob.training.web1902.koryukinr.common.*;
import com.getjavajob.training.web1902.koryukinr.dao.AccountDAO;
import com.getjavajob.training.web1902.koryukinr.dao.GroupDAO;
import com.getjavajob.training.web1902.koryukinr.dao.GroupMembershipDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

@Service
public class GroupService {
    @Autowired
    private GroupDAO groupDAO;
    @Autowired
    private GroupMembershipDAO groupMembershipDAO;
    @Autowired
    private AccountDAO accountDAO;

    @Transactional
    public Group createGroup(Group group) {
        group.setCreatedDate(LocalDate.now());
        Group added = groupDAO.save(group);
        groupMembershipDAO.save(new GroupMembership(group.getId(), group.getOwner(), true, true));
        return added;
    }

    @Transactional
    public void deleteCroup(Group group) {
        groupDAO.delete(group);
    }

    @Transactional
    public void updateGroup(Group group) {
        groupDAO.save(group);
    }

    @Transactional
    public void updateGroupMembership(GroupMembership groupMembership) {
        groupMembershipDAO.save(groupMembership);
    }

    @Transactional
    public void sendGroupRequest(int accountId, int groupId) {
        groupMembershipDAO.save(new GroupMembership(groupId, accountId, false, false));
    }

    public Group findGroupById(int id) {
        return groupDAO.findById(id);
    }

    @Transactional
    public void deleteGroupMembership(int accountId, int groupId) {
        groupMembershipDAO.deleteByAccountIdAndGroupId(accountId, groupId);
    }

    @Transactional
    public GroupMembership getGroupMembershipByAccountIdAndGroupId(int accountId, int groupId) {
        return groupMembershipDAO.findByAccountIdAndGroupId(accountId, groupId);
    }

    public List<Group> getGroupsWithMe(int accountId) {
        List<GroupMembership> memberships = groupMembershipDAO.findAll();
        List<Group> groups = new ArrayList<>();
        for (GroupMembership membership : memberships) {
            if (membership.getAccountId() == accountId && membership.isAccept()) {
                groups.add(groupDAO.findById(membership.getGroupId()));
            }
        }
        return groups;
    }

    public List<Group> sendRequestGroupsByMe(int accountId) {
        List<GroupMembership> memberships = groupMembershipDAO.findAll();
        List<Group> groups = new ArrayList<>();
        for (GroupMembership membership : memberships) {
            if (membership.getAccountId() == accountId && !membership.isAccept()) {
                groups.add(groupDAO.findById(membership.getGroupId()));
            }
        }
        return groups;
    }

    public List<Account> getGroupMembershipRequestAccounts(int groupId) {
        List<GroupMembership> groupMemberships = groupMembershipDAO.findAllByGroupId(groupId);
        List<Account> accounts = new ArrayList<>();
        for (GroupMembership groupMembership : groupMemberships) {
            if (!groupMembership.isAccept()) {
                accounts.add(accountDAO.findById(groupMembership.getAccountId()));
            }
        }
        return accounts;
    }

    public List<Account> getGroupMembershipUserAndAdminAccounts(int groupId) {
        List<GroupMembership> groupMemberships = groupMembershipDAO.findAllByGroupId(groupId);
        List<Account> accounts = new ArrayList<>();
        for (GroupMembership groupMembership : groupMemberships) {
            if (groupMembership.isAccept()) {
                accounts.add(accountDAO.findById(groupMembership.getAccountId()));
            }
        }
        return accounts;
    }

    public List<Account> getGroupMembershipAllAccounts(int groupId) {
        List<GroupMembership> groupMemberships = groupMembershipDAO.findAllByGroupId(groupId);
        List<Account> accounts = new ArrayList<>();
        for (GroupMembership groupMembership : groupMemberships) {
            accounts.add(accountDAO.findById(groupMembership.getAccountId()));
        }
        return accounts;
    }

    public List<Account> getGroupMembershipUserAccounts(int groupId) {
        List<GroupMembership> groupMemberships = groupMembershipDAO.findAllByGroupId(groupId);
        List<Account> accounts = new ArrayList<>();
        for (GroupMembership groupMembership : groupMemberships) {
            if (!groupMembership.isAdmin() && groupMembership.isAccept()) {
                accounts.add(accountDAO.findById(groupMembership.getAccountId()));
            }
        }
        return accounts;
    }

    public List<Account> getGroupMembershipAdminAccounts(int groupId) {
        List<GroupMembership> groupMemberships = groupMembershipDAO.findAllByGroupId(groupId);
        List<Account> accounts = new ArrayList<>();
        for (GroupMembership groupMembership : groupMemberships) {
            if (groupMembership.isAdmin()) {
                accounts.add(accountDAO.findById(groupMembership.getAccountId()));
            }
        }
        return accounts;
    }

    public boolean checkOwnerGroup(int groupId, int ownerId) {
        Group group = groupDAO.findById(groupId);
        return group.getOwner() == ownerId;
    }

    public boolean checkUserGroupMembership(int groupId, int accountId) {
        List<GroupMembership> groupMemberships = groupMembershipDAO.findAllByGroupId(groupId);
        for (GroupMembership groupMembership : groupMemberships) {
            if (groupMembership.isAccept() && groupMembership.getAccountId() == accountId) {
                return true;
            }
        }
        return false;
    }

    public boolean checkAdminGroupMembership(int groupId, int accountId) {
        List<GroupMembership> groupMemberships = groupMembershipDAO.findAllByGroupId(groupId);
        for (GroupMembership groupMembership : groupMemberships) {
            if (groupMembership.isAdmin() && groupMembership.getAccountId() == accountId) {
                return true;
            }
        }
        return false;
    }

    public boolean checkNewGroupMembership(int groupId, int accountId) {
        List<GroupMembership> groupMemberships = groupMembershipDAO.findAllByGroupId(groupId);
        for (GroupMembership groupMembership : groupMemberships) {
            if (groupMembership.getAccountId() == accountId) {
                return false;
            }
        }
        return true;
    }

    public List<Group> getFoundGroups(String search) {
        List<Group> allGroups = groupDAO.findAll();
        List<Group> foundGroups = new ArrayList<>();
        ListIterator<Group> listIterator = allGroups.listIterator();
        Group temp;
        while (listIterator.hasNext()) {
            temp = listIterator.next();
            String name = temp.getName().toLowerCase();
            if (name.contains(search.toLowerCase())) {
                foundGroups.add(temp);
            }
        }
        return foundGroups;
    }

    public List<Group> calculateGroupsByPage(int page, List<Group> foundGroups) {
        List<Group> fiveGroupsOnPage = new ArrayList<>();
        int sizeFoundGroups = foundGroups.size();
        int sizeGroupsOnPage = 5;
        int allPagesForGroups = (sizeFoundGroups / sizeGroupsOnPage) + 1;

        if (page == allPagesForGroups) {
            int k;
            for (int j = (allPagesForGroups - 1) * sizeGroupsOnPage; j < sizeFoundGroups; j++) {
                fiveGroupsOnPage.add(foundGroups.get(j));
            }
        } else {
            for (int k = (page - 1) * sizeGroupsOnPage; k < page * sizeGroupsOnPage; k++) {
                fiveGroupsOnPage.add(foundGroups.get(k));
            }
        }
        return fiveGroupsOnPage;
    }

    public List<Group> getAll() {
        return groupDAO.findAll();
    }

    public Group findByName(String name) {
        return groupDAO.findByName(name);
    }

    public List<Group> findGroupByOwnerId(int owner) {
        return groupDAO.findAllByOwner(owner);
    }

    public Group getGroupById(int id) {
        return groupDAO.findById(id);
    }
}
