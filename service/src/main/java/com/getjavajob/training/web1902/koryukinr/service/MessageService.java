package com.getjavajob.training.web1902.koryukinr.service;

import com.getjavajob.training.web1902.koryukinr.common.Account;
import com.getjavajob.training.web1902.koryukinr.common.Message;
import com.getjavajob.training.web1902.koryukinr.common.MessageType;
import com.getjavajob.training.web1902.koryukinr.dao.MessageDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class MessageService {
    @Autowired
    private MessageDAO messageDAO;

    @Transactional
    public void saveMessage(Message message) {
        messageDAO.save(message);
    }

    @Transactional
    public void sendMessageToGroupWall(Message message) {
        message.setMessageType(MessageType.GROUP_WALL);
        message.setLocalDate(LocalDateTime.now());
        messageDAO.save(message);
    }

    @Transactional
    public void sendPrivateMessage(Message message) {
        message.setMessageType(MessageType.PRIVATE);
        message.setLocalDate(LocalDateTime.now());
        messageDAO.save(message);
    }

    @Transactional
    public void sendMessageToAccountWall(Message message) {
        message.setMessageType(MessageType.ACCOUNT_WALL);
        message.setLocalDate(LocalDateTime.now());
        messageDAO.save(message);
    }

    @Transactional
    public void updateMessage(Message message) {
        messageDAO.save(message);
    }

    @Transactional
    public void deleteMessage(Message message) {
        messageDAO.delete(message);
    }

    public List<Message> findAllMessagesByIdTo(int id, MessageType type) {
        return messageDAO.findAllByMessageTypeAndIdToOrderByLocalDateDesc(type, id);
    }

    public List<Message> findAllMessagesBetweenTwoAccounts(Account account, int idTwo) {
        return messageDAO.findAllMessagesBetweenAccountFromAndIdTwoByMessageType(account, idTwo);
    }

    public Message findById(int id) {
        return messageDAO.findById(id);
    }

}
