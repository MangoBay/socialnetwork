package com.getjavajob.training.web1902.koryukinr.service;

import com.getjavajob.training.web1902.koryukinr.common.*;
import com.getjavajob.training.web1902.koryukinr.dao.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

@Service
public class AccountService {
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private FriendDAO friendDAO;
    @Autowired
    private MessageDAO messageDAO;

    @Transactional
    public Account createAccount(Account account) {
        account.setRegistrationDate(LocalDate.now());
        return accountDAO.save(account);
    }

    @Transactional
    public void updateAccount(Account account) {
        accountDAO.save(account);
    }

    @Secured("ROLE_ADMIN")
    @Transactional
    public void deleteAccount(Account account) {
        messageDAO.deleteAllByAccountFromOrIdTo(account, account.getId());
        accountDAO.delete(account);
    }

    public Account findByMailAndPass(String mail, String pass) {
        return accountDAO.findByEmailAndPassword(mail, pass);
    }

    public Account findByMail(String mail) {
        return accountDAO.findByEmail(mail);
    }

    public Account getById(int id) {
        return accountDAO.findById(id);
    }

    public List<Account> getAllAccounts() {
        return accountDAO.findAll();
    }

    @Transactional
    public void sendFriendRequest(int accountIdFrom, int accountIdTo) {
        friendDAO.save(new Friendship(accountIdFrom, accountIdTo, Status.PENDING, accountIdFrom));
    }

    public boolean checkFriendship(Account account, Account friend) {
        Friendship friendshipFrom = friendDAO.findFriendshipByIdFromAndIdTo(account.getId(), friend.getId());
        if (friendshipFrom != null && (friendshipFrom.getStatus() == Status.ACCEPTED ||
                friendshipFrom.getActionId() == account.getId())) {
            return true;
        }
        Friendship friendshipTo = friendDAO.findFriendshipByIdFromAndIdTo(friend.getId(), account.getId());
        return friendshipTo != null && (friendshipTo.getStatus() == Status.ACCEPTED ||
                friendshipTo.getActionId() == account.getId());
    }

    public boolean checkRequestToMe(Account account, Account friend) {
        Friendship friendshipTo = friendDAO.findFriendshipByIdFromAndIdTo(friend.getId(), account.getId());
        return friendshipTo != null && friendshipTo.getActionId() == friend.getId();
    }

    public boolean checkResponseToMe(Account account, Account friend) {
        Friendship friendshipTo = friendDAO.findFriendshipByIdFromAndIdTo(account.getId(), friend.getId());
        return friendshipTo != null && friendshipTo.getActionId() == account.getId() &&
                friendshipTo.getStatus() == Status.PENDING;
    }

    @Transactional
    public void acceptFriendRequest(int accountIdFrom, int accountIdTo) {
        Friendship friendshipFrom = friendDAO.findFriendshipByIdFromAndIdTo(accountIdFrom, accountIdTo);
        Friendship friendshipTo = friendDAO.findFriendshipByIdFromAndIdTo(accountIdTo, accountIdFrom);
        if (friendshipFrom != null) {
            friendshipFrom.setStatus(Status.ACCEPTED);
            friendshipFrom.setActionId(accountIdFrom);
            friendDAO.save(friendshipFrom);
        } else if (friendshipTo != null) {
            friendshipTo.setStatus(Status.ACCEPTED);
            friendshipTo.setIdFrom(accountIdFrom);
            friendshipTo.setIdTo(accountIdTo);
            friendshipTo.setActionId(accountIdFrom);
            friendDAO.save(friendshipTo);
        }
    }

    @Transactional
    public void deleteFriendship(int accountIdFrom, int accountIdTo) {
        Friendship friendshipFrom = friendDAO.findFriendshipByIdFromAndIdTo(accountIdFrom, accountIdTo);
        Friendship friendshipTo = friendDAO.findFriendshipByIdFromAndIdTo(accountIdTo, accountIdFrom);
        if (friendshipFrom != null) {
            friendDAO.delete(friendshipFrom);
        } else if (friendshipTo != null) {
            friendDAO.delete(friendshipTo);
        }
    }

    public List<Account> getFriends(Account account) {
        List<Account> friends = new ArrayList<>();
        int id = account.getId();
        List<Friendship> friendships = friendDAO.findAllByIdFromOrIdTo(id, id);
        for (Friendship friendship : friendships) {
            if (friendship.getStatus() == Status.ACCEPTED && friendship.getIdFrom() == account.getId()) {
                friends.add(accountDAO.findById(friendship.getIdTo()));
            }
            if (friendship.getStatus() == Status.ACCEPTED && friendship.getIdTo() == account.getId()) {
                friends.add(accountDAO.findById(friendship.getIdFrom()));
            }
        }
        return friends;
    }

    public List<Account> sendRequests(Account account) {
        List<Account> requests = new ArrayList<>();
        int id = account.getId();
        List<Friendship> friendships = friendDAO.findAllByIdFromOrIdTo(id, id);
        for (Friendship friendship : friendships) {
            if (friendship.getStatus() == Status.PENDING && friendship.getActionId() == account.getId()) {
                requests.add(accountDAO.findById(friendship.getIdTo()));
            }
        }
        return requests;
    }

    public List<Account> acceptRequests(Account account) {
        List<Account> requests = new ArrayList<>();
        int id = account.getId();
        List<Friendship> friendships = friendDAO.findAllByIdFromOrIdTo(id, id);
        for (Friendship friendship : friendships) {
            if (friendship.getStatus() == Status.PENDING && friendship.getActionId() != account.getId()) {
                requests.add(accountDAO.findById(friendship.getActionId()));
            }
        }
        return requests;
    }

    public List<Account> getFoundAccounts(String search) {
        List<Account> allAccounts = accountDAO.findAll();
        List<Account> foundAccounts = new ArrayList<>();
        ListIterator<Account> listIterator = allAccounts.listIterator();
        String searchLowerCase = search.toLowerCase();
        Account temp;
        while (listIterator.hasNext()) {
            temp = listIterator.next();
            String firstName = temp.getFirstName();
            String lastName = temp.getLastName();
            if (firstName.toLowerCase().contains(searchLowerCase) || lastName.toLowerCase().contains(searchLowerCase)) {
                foundAccounts.add(temp);
            }
        }
        return foundAccounts;
    }

    public List<Account> calculateAccountsByPage(int page, List<Account> foundAccounts) {
        List<Account> fiveAccountsOnPage = new ArrayList<>();
        int sizeFoundAccounts = foundAccounts.size();
        int sizeAccountsOnPage = 5;
        int allPagesForAccounts = (sizeFoundAccounts / sizeAccountsOnPage) + 1;

        if (page == allPagesForAccounts) {
            for (int j = (allPagesForAccounts - 1) * sizeAccountsOnPage; j < sizeFoundAccounts; j++) {
                fiveAccountsOnPage.add(foundAccounts.get(j));
            }
        } else {
            for (int k = (page - 1) * sizeAccountsOnPage; k < page * sizeAccountsOnPage; k++) {
                fiveAccountsOnPage.add(foundAccounts.get(k));
            }
        }
        return fiveAccountsOnPage;
    }

    public void addPhones(Account account, String[] phones) {
        if (phones != null && phones.length != 0) {
            List<Phone> newPhones = new ArrayList<>();
            for (String phoneNumber : phones) {
                newPhones.add(new Phone(account.getId(), phoneNumber));
            }
            account.setPhones(newPhones);
        }

        List<Phone> allPhones = account.getPhones();
        if (phones == null && !allPhones.isEmpty()) {
            Iterator<Phone> iterator = allPhones.iterator();
            while (iterator.hasNext()) {
                iterator.next();
                iterator.remove();
            }
        }
    }

    public void setPropertiesAccount(Account account, Account xmlAccount) {
        if (xmlAccount.getPhones() != null) {
            account.setPhones(xmlAccount.getPhones());
        }
        if (xmlAccount.getFirstName() != null) {
            account.setFirstName(xmlAccount.getFirstName());
        }
        if (xmlAccount.getLastName() != null) {
            account.setLastName(xmlAccount.getLastName());
        }
        if (xmlAccount.getPassword() != null) {
            account.setPassword(xmlAccount.getPassword());
        }
        if (xmlAccount.getEmail() != null) {
            account.setEmail(xmlAccount.getEmail());
        }
        if (xmlAccount.getBirthDate() != null) {
            account.setBirthDate(xmlAccount.getBirthDate());
        }
    }
}
