package com.getjavajob.training.web1902.koryukinr.service;

import com.getjavajob.training.web1902.koryukinr.common.Group;
import com.getjavajob.training.web1902.koryukinr.dao.GroupDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@RunWith(MockitoJUnitRunner.class)
public class GroupServiceTest {
    @Mock
    private GroupDAO groupDAO;
    @InjectMocks
    private GroupService groupService;
    private Group group;

    @Before
    public void init() {
        group = new Group("Art", 3, "Something", LocalDate.now());
    }

//    @Test
//    public void createGroup() {
//        when(groupDAO.save(group)).thenReturn(group);
//        Group groupActual = groupService.createGroup(group);
//        assertEquals(group, groupActual);
//        verify(groupDAO, times(1)).save(group);
//        verifyNoMoreInteractions(groupDAO);
//    }

    @Test
    public void updateGroup() {
        groupService.updateGroup(group);
        verify(groupDAO).save(group);
        verifyNoMoreInteractions(groupDAO);
    }

//    @Test
//    public void deleteGroup() {
//        groupService.deleteCroup(group.getId());
//        verify(groupDAO).deleteById(group.getId());
//        verifyNoMoreInteractions(groupDAO);
//    }

    @Test
    public void getAll() {
        List<Group> expectedGroups = new ArrayList<>();
        expectedGroups.add(group);
        when(groupDAO.findAll()).thenReturn(expectedGroups);

        List<Group> actualGroups = groupService.getAll();
        assertEquals(expectedGroups, actualGroups);
    }

    @Test
    public void findByName() {
        when(groupDAO.findByName("Art")).thenReturn(group);

        Group actual = groupDAO.findByName("Art");
        assertEquals(group, actual);
    }

    @Test
    public void findGroupByOwnerId() {
        List<Group> expectedGroups = new ArrayList<>();
        expectedGroups.add(group);
        when(groupDAO.findAllByOwner(3)).thenReturn(expectedGroups);

        List<Group> actualGroups = groupService.findGroupByOwnerId(3);
        assertEquals(expectedGroups, actualGroups);
    }

    @Test
    public void getById() {
        when(groupDAO.findById(10)).thenReturn(group);

        Group actual = groupService.getGroupById(10);
        assertEquals(group, actual);
    }
}
