package com.getjavajob.training.web1902.koryukinr.service;

import com.getjavajob.training.web1902.koryukinr.common.*;
import com.getjavajob.training.web1902.koryukinr.dao.AccountDAO;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
    @Mock
    private AccountDAO accountDAO;
    @InjectMocks
    private AccountService accountService;

    private Account account1;

    @Before
    public void init() {
        LocalDate date = LocalDate.now();
        account1 = new Account("Roman", "Koryukin", "roman@mail.ru", "romka", date, date);
    }

    @Test
    public void createAccount() {
        List<Phone> phones = new ArrayList<>();
        Phone phone = new Phone(1, "930-213");
        phones.add(phone);
        account1.setPhones(phones);

        when(accountDAO.save(account1)).thenReturn(account1);

        Account actual = accountService.createAccount(account1);
        assertEquals(account1, actual);
        verify(accountDAO).save(account1);
        verifyNoMoreInteractions(accountDAO);
    }

    @Test
    public void updateAccount() {
        accountService.updateAccount(account1);
        verify(accountDAO).save(account1);

        verifyNoMoreInteractions(accountDAO);
    }
}
