package com.getjavajob.training.web1902.koryukinr.common;

import java.io.Serializable;

public class Notification implements Serializable {
    private static final long serialVersionUID = -4526734244466327723L;

    private int id;
    private String mail;
    private String message;

    public Notification() {
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
