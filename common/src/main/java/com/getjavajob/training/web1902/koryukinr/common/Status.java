package com.getjavajob.training.web1902.koryukinr.common;

public enum Status {
    PENDING, ACCEPTED;

    Status() {
    }
}
