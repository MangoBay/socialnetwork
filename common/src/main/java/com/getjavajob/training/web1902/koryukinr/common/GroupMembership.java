package com.getjavajob.training.web1902.koryukinr.common;

import javax.persistence.*;

@Entity
@Table(name = "group_membership")
public class GroupMembership {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "group_id")
    private int groupId;

    @Column(name = "account_id")
    private int accountId;

    @Column(name = "is_admin")
    private boolean isAdmin;

    @Column(name = "is_accept")
    private boolean isAccept;

    public GroupMembership() {
    }

    public GroupMembership(int groupId, int accountId, boolean isAdmin, boolean isAccept) {
        this.groupId = groupId;
        this.accountId = accountId;
        this.isAdmin = isAdmin;
        this.isAccept = isAccept;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    public boolean isAccept() {
        return isAccept;
    }

    public void setAccept(boolean accept) {
        isAccept = accept;
    }

    @Override
    public String toString() {
        return "GroupMembership{" +
                "id=" + id +
                ", groupId=" + groupId +
                ", accountId=" + accountId +
                ", isAdmin=" + isAdmin +
                ", isAccept=" + isAccept +
                '}';
    }
}
