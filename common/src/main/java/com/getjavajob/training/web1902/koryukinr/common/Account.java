package com.getjavajob.training.web1902.koryukinr.common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;
import com.thoughtworks.xstream.annotations.XStreamOmitField;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Entity
@Table(name = "account")
@XStreamAlias("Account")
public class Account {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    @XStreamAlias("id")
    private int id;

    @XStreamImplicit(itemFieldName = "phones")
    @OneToMany(mappedBy = "accountId", orphanRemoval = true, cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private List<Phone> phones = new ArrayList<>();

    @Column(name = "additional_information")
    @XStreamOmitField
    private String additionalInfo;

    @Column(name = "photo")
    @XStreamOmitField
    private byte[] photo;

    @Column(name = "birth_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XStreamAlias("birthDate")
    private LocalDate birthDate;

    @Column(name = "registration_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @XStreamAlias("registrationDate")
    private LocalDate registrationDate;

    @Column(name = "first_name")
    @XStreamAlias("firstName")
    private String firstName;

    @Column(name = "middle_name")
    @XStreamOmitField
    private String middleName;

    @Column(name = "last_name")
    @XStreamAlias("lastName")
    private String lastName;

    @Column(name = "home_address")
    @XStreamOmitField
    private String homeAddress;

    @Column(name = "work_address")
    @XStreamOmitField
    private String workAddress;

    @Column(name = "email")
    @XStreamAlias("email")
    private String email;

    @Column(name = "password")
    @XStreamAlias("password")
    private String password;

    @Column(name = "skype")
    @XStreamOmitField
    private String skype;

    @Column(name = "is_admin")
    @XStreamAlias("isAdmin")
    private boolean isAdmin;

    public Account() {
    }

    public Account(String firstName, String lastName, String email, String password) {
        this(firstName, lastName, email, password, null, null);
    }

    public Account(String firstName, String lastName, String email, String password, LocalDate birthDate,
                   LocalDate registrationDate) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.birthDate = birthDate;
        this.registrationDate = registrationDate;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getHomeAddress() {
        return homeAddress;
    }

    public void setHomeAddress(String homeAddress) {
        this.homeAddress = homeAddress;
    }

    public String getWorkAddress() {
        return workAddress;
    }

    public void setWorkAddress(String workAddress) {
        this.workAddress = workAddress;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public boolean isAdmin() {
        return isAdmin;
    }

    public void setAdmin(boolean admin) {
        isAdmin = admin;
    }

    @Override
    public String toString() {
        return "Account{" +
                "id=" + id +
                ", phones=" + phones +
                ", additionalInfo='" + additionalInfo + '\'' +
                ", photo=" + Arrays.toString(photo) +
                ", birthDate=" + birthDate +
                ", registrationDate=" + registrationDate +
                ", firstName='" + firstName + '\'' +
                ", middleName='" + middleName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", homeAddress='" + homeAddress + '\'' +
                ", workAddress='" + workAddress + '\'' +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", skype='" + skype + '\'' +
                ", isAdmin=" + isAdmin +
                '}';
    }
}
