package com.getjavajob.training.web1902.koryukinr.common;

public enum MessageType {
    PRIVATE, GROUP_WALL, ACCOUNT_WALL;

    MessageType() {
    }
}
