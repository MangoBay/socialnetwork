package com.getjavajob.training.web1902.koryukinr.common;

import javax.persistence.*;

@Entity
@Table(name = "friendship")
public class Friendship {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "account_id_from")
    private int idFrom;

    @Column(name = "account_id_to")
    private int idTo;

    @Column(name = "action_user_id")
    private int actionId;

    @Column(name = "status")
    @Enumerated(EnumType.STRING)
    private Status status;

    public Friendship() {
    }

    public Friendship(int idFrom, int idTo) {
        this.idFrom = idFrom;
        this.idTo = idTo;
    }

    public Friendship(int idFrom, int idTo, Status status, int actionId) {
        this.idFrom = idFrom;
        this.idTo = idTo;
        this.actionId = actionId;
        this.status = status;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(int idFrom) {
        this.idFrom = idFrom;
    }

    public int getIdTo() {
        return idTo;
    }

    public void setIdTo(int idTo) {
        this.idTo = idTo;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "id=" + id +
                ", idFrom=" + idFrom +
                ", idTo=" + idTo +
                ", actionId=" + actionId +
                ", status=" + status +
                '}';
    }
}
