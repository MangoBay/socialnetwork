package com.getjavajob.training.web1902.koryukinr.webapp.utils;

public class CustomMessage {
    private int idFrom;
    private int idTo;
    private String message;
    private String date;

    public CustomMessage() {
    }

    public CustomMessage(int idFrom, int idTo, String message) {
        this.idFrom = idFrom;
        this.idTo = idTo;
        this.message = message;
    }

    public int getIdFrom() {
        return idFrom;
    }

    public void setIdFrom(int idFrom) {
        this.idFrom = idFrom;
    }

    public int getIdTo() {
        return idTo;
    }

    public void setIdTo(int idTo) {
        this.idTo = idTo;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
