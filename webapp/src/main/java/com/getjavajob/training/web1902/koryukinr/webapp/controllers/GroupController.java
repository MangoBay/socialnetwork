package com.getjavajob.training.web1902.koryukinr.webapp.controllers;

import com.getjavajob.training.web1902.koryukinr.common.*;
import com.getjavajob.training.web1902.koryukinr.service.GroupService;
import com.getjavajob.training.web1902.koryukinr.service.MessageService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

import static com.getjavajob.training.web1902.koryukinr.webapp.utils.CommonUtils.fillInitBinder;
import static com.getjavajob.training.web1902.koryukinr.webapp.utils.CommonUtils.setContentType;

@Controller
public class GroupController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private GroupService groupService;
    @Autowired
    private MessageService messageService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        logger.trace("Init common properties");
        fillInitBinder(binder);
    }

    @RequestMapping(value = "/group", method = RequestMethod.GET)
    public ModelAndView group(@RequestParam("id") int id, HttpSession httpSession) {
        logger.info("Data generation for group with id = {}", id);
        Account account = (Account) httpSession.getAttribute("account");
        ModelAndView modelAndView = new ModelAndView("groupDescription");
        Group group = groupService.getGroupById(id);
        List<Account> groupMembershipAllAccounts = groupService.getGroupMembershipAllAccounts(id);
        List<Account> groupMembershipRequestAccounts = groupService.getGroupMembershipRequestAccounts(id);
        List<Account> groupMembershipUserAccounts = groupService.getGroupMembershipUserAccounts(id);
        List<Account> groupMembershipUserAndAdminAccounts = groupService.getGroupMembershipUserAndAdminAccounts(id);
        List<Account> groupMembershipAdminAccounts = groupService.getGroupMembershipAdminAccounts(id);
        List<Message> messages = messageService.findAllMessagesByIdTo(id, MessageType.GROUP_WALL);

        modelAndView.addObject("groupMembershipAllAccounts", groupMembershipAllAccounts);
        modelAndView.addObject("groupMembershipRequestAccounts", groupMembershipRequestAccounts);
        modelAndView.addObject("groupMembershipUserAccounts", groupMembershipUserAccounts);
        modelAndView.addObject("groupMembershipUserAndAdminAccounts", groupMembershipUserAndAdminAccounts);
        modelAndView.addObject("groupMembershipAdminAccounts", groupMembershipAdminAccounts);
        modelAndView.addObject("messageWall", messages);

        modelAndView.addObject("isAdmin", groupService.checkAdminGroupMembership(id, account.getId()));
        modelAndView.addObject("isUser", groupService.checkUserGroupMembership(id, account.getId()));
        modelAndView.addObject("isNewMembership", groupService.checkNewGroupMembership(id, account.getId()));
        modelAndView.addObject("isOwner", groupService.checkOwnerGroup(id, account.getId()));
        modelAndView.addObject("group", group);
        return modelAndView;
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.GET)
    public ModelAndView createGroup() {
        logger.info("Create new group and redirect on view for fill data");
        return new ModelAndView("createGroup", "group", new Group());
    }

    @RequestMapping(value = "/createGroup", method = RequestMethod.POST)
    public String doCreateGroup(@ModelAttribute Group group, HttpSession httpSession) {
        logger.info("Create new group = {}", group);
        Account account = (Account) httpSession.getAttribute("account");
        group.setOwner(account.getId());
        groupService.createGroup(group);
        Group added = groupService.findByName(group.getName());
        return "redirect: /group?id=" + added.getId();
    }

    @RequestMapping(value = "/editGroup", method = RequestMethod.GET)
    public ModelAndView editGroup(@RequestParam int id) {
        logger.info("Redirect group(id = {}) for save data", id);
        Group group = groupService.getGroupById(id);
        return new ModelAndView("editGroup", "group", group);
    }

    @RequestMapping(value = "/updateGroup", method = RequestMethod.POST)
    public String doEditGroup(@ModelAttribute Group group, @RequestParam int id) {
        logger.info("Update group = {}", group);
        Group groupBeforeEdit = groupService.getGroupById(id);
        group.setOwner(groupBeforeEdit.getOwner());
        group.setId(groupBeforeEdit.getId());
        group.setCreatedDate(groupBeforeEdit.getCreatedDate());
        if (group.getPhoto().length == 0 && groupBeforeEdit.getPhoto().length != 0) {
            group.setPhoto(groupBeforeEdit.getPhoto());
        }
        groupService.updateGroup(group);
        return "redirect: /group?id=" + group.getId();
    }

    @RequestMapping(value = "/deleteGroup", method = RequestMethod.GET)
    public String deleteGroup(@RequestParam int id) {
        logger.info("Delete group = {}", id);
        groupService.deleteCroup(groupService.findGroupById(id));
        return "redirect: /accountGroups";
    }

    @RequestMapping(value = "/groupPrintImage", method = RequestMethod.GET)
    public void groupPrintImage(HttpServletResponse response, @RequestParam("groupId") int id) throws IOException {
        logger.info("Print image for group(id = {])", id);
        byte[] image;
        Group group = groupService.getGroupById(id);
        image = group.getPhoto();
        setContentType(response, image);
    }

    @RequestMapping(value = "/searchGroupsByPageAjax", method = RequestMethod.GET)
    @ResponseBody
    public List<Group> searchGroupByPageAjax(@RequestParam String search, @RequestParam int page) {
        logger.info("Search = {} groups by page = {} with ajax", search, page);
        List<Group> foundGroups = groupService.getFoundGroups(search);
        return groupService.calculateGroupsByPage(page, foundGroups);
    }

    @RequestMapping(value = "/requestGroupMembership", method = RequestMethod.GET)
    public String requestGroupMembership(@RequestParam int id, HttpSession httpSession) {
        logger.info("GroupMembership(id = {}) requests", id);
        Account account = (Account) httpSession.getAttribute("account");
        groupService.sendGroupRequest(account.getId(), id);
        return "redirect: /group?id=" + id;
    }

    @RequestMapping(value = "/deleteGroupMembership", method = RequestMethod.GET)
    public String deleteGroupMembership(@RequestParam int groupId, @RequestParam int accountId) {
        logger.info("Delete groupMembership with param (groupId = {}, accountId = {})", groupId, accountId);
        groupService.deleteGroupMembership(accountId, groupId);
        return "redirect: /group?id=" + groupId;
    }

    @RequestMapping(value = "/acceptGroupMembershipRequest", method = RequestMethod.GET)
    public String acceptGroupMembershipRequest(@RequestParam int groupId, @RequestParam int accountId) {
        logger.info("Accept groupMembership with param (groupId = {}, accountId = {})", groupId, accountId);
        GroupMembership groupMembership = groupService.getGroupMembershipByAccountIdAndGroupId(accountId, groupId);
        groupMembership.setAccept(true);
        groupService.updateGroupMembership(groupMembership);
        return "redirect: /group?id=" + groupId;
    }

    @RequestMapping(value = "/makeAdminByGroupAndAccount", method = RequestMethod.GET)
    public String makeAdmin(@RequestParam int groupId, @RequestParam int accountId) {
        logger.info("Make admin groupMembership with param (groupId = {}, accountId = {})", groupId, accountId);
        GroupMembership groupMembership = groupService.getGroupMembershipByAccountIdAndGroupId(accountId, groupId);
        groupMembership.setAdmin(true);
        groupService.updateGroupMembership(groupMembership);
        return "redirect: /group?id=" + groupId;
    }

    @RequestMapping(value = "/deleteFromAdmin", method = RequestMethod.GET)
    public String deleteFromAdmin(@RequestParam int groupId, @RequestParam int accountId) {
        logger.info("Delete admin groupMembership with param (groupId = {}, accountId = {})", groupId, accountId);
        GroupMembership groupMembership = groupService.getGroupMembershipByAccountIdAndGroupId(accountId, groupId);
        groupMembership.setAdmin(false);
        groupService.updateGroupMembership(groupMembership);
        return "redirect: /group?id=" + groupId;
    }
}
