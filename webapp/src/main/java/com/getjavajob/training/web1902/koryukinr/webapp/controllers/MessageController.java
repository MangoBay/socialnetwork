package com.getjavajob.training.web1902.koryukinr.webapp.controllers;

import com.getjavajob.training.web1902.koryukinr.common.Account;
import com.getjavajob.training.web1902.koryukinr.common.Message;
import com.getjavajob.training.web1902.koryukinr.service.AccountService;
import com.getjavajob.training.web1902.koryukinr.service.MessageService;
import com.getjavajob.training.web1902.koryukinr.webapp.utils.CustomMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;

import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.getjavajob.training.web1902.koryukinr.webapp.utils.CommonUtils.fillInitBinder;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@Controller
public class MessageController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private MessageService messageService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        logger.trace("Init common properties");
        fillInitBinder(binder);
    }

    @RequestMapping(value = "/sendMessageToWall", method = POST)
    public String sendMessageToWall(@RequestParam(value = "idToGroupWall", required = false) Integer idToGroupWall,
                                    @RequestParam(value = "idToAccountWall", required = false) Integer idToAccountWall,
                                    @RequestParam("message") String messageText,
                                    @SessionAttribute("account") Account account) {
        Message message = new Message();
        message.setAccountFrom(account);
        message.setMessage(messageText);

        if (idToAccountWall != null) {
            message.setIdTo(idToAccountWall);
            messageService.sendMessageToAccountWall(message);
            return "redirect: /account?id=" + message.getIdTo();
        } else if (idToGroupWall != null) {
            message.setIdTo(idToGroupWall);
            messageService.sendMessageToGroupWall(message);
            return "redirect: /group?id=" + message.getIdTo();
        } else {
            return "redirect: /account?id=" + account.getId();
        }
    }

    @RequestMapping(value = "/removeMessageFromWall", method = POST)
    public String removeMessageFromWall(@RequestParam(value = "id") int id,
                                        @RequestParam(value = "text", required = false) String textMessage,
                                        @SessionAttribute("account") Account account) {
        Message message = messageService.findById(id);
        if (textMessage == null) {
            messageService.deleteMessage(message);
        } else {
            message.setMessage(textMessage);
            messageService.updateMessage(message);
        }

        int messageIdTo = message.getIdTo();
        switch (message.getMessageType()) {
            case GROUP_WALL:
                return "redirect: /group?id=" + messageIdTo;
            case ACCOUNT_WALL:
                return "redirect: /account?id=" + messageIdTo;
            default:
                return "redirect: /account?id=" + account.getId();
        }
    }


    @MessageMapping("/chat/{idFrom}/{idTo}")
    @SendTo("/topic/{idFrom}/{idTo}")
    public CustomMessage sendMessage(@Payload CustomMessage customMessage) {
        Account account = accountService.getById(customMessage.getIdFrom());
        Message saveMessage = new Message(account, customMessage.getIdTo(), customMessage.getMessage());
        messageService.sendPrivateMessage(saveMessage);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String date = saveMessage.getLocalDate().format(formatter);

        customMessage.setDate(date);
        return customMessage;
    }

    @ResponseBody
    @RequestMapping("/getMessageHistory")
    public List<Message> getMessageHistory(@SessionAttribute("account") Account account, @RequestParam int id) {
        return messageService.findAllMessagesBetweenTwoAccounts(account, id);
    }
}
