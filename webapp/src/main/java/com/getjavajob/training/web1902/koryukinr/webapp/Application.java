package com.getjavajob.training.web1902.koryukinr.webapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Configuration
@SpringBootApplication(scanBasePackages = "com.getjavajob.training.web1902.koryukinr")
@EntityScan(basePackages = "com.getjavajob.training.web1902.koryukinr.common")
@EnableJpaRepositories(basePackages = "com.getjavajob.training.web1902.koryukinr.dao")
public class Application extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }

}