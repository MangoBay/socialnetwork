package com.getjavajob.training.web1902.koryukinr.webapp.security;

import com.getjavajob.training.web1902.koryukinr.common.Account;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
                                        Authentication authentication) throws IOException {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth instanceof AnonymousAuthenticationToken) {
            return;
        }
        Account account = ((CustomUserDetailsImpl) auth.getPrincipal()).getAccount();
        response.setStatus(HttpServletResponse.SC_OK);
        if (account != null) {
            request.getSession().setAttribute("account", account);
            response.sendRedirect("/account?id=" + account.getId());
        }
    }
}