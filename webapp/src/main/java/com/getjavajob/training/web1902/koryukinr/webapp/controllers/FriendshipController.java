package com.getjavajob.training.web1902.koryukinr.webapp.controllers;

import com.getjavajob.training.web1902.koryukinr.common.Account;
import com.getjavajob.training.web1902.koryukinr.service.AccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttribute;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
public class FriendshipController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "/friends", method = RequestMethod.GET)
    public ModelAndView friends(@SessionAttribute("account") Account account) {
        logger.info("Data generation for friends with id = {}", account.getId());
        ModelAndView modelAndView = new ModelAndView("friends");
        List<Account> friends = accountService.getFriends(account);
        List<Account> sendRequests = accountService.sendRequests(account);
        List<Account> acceptRequests = accountService.acceptRequests(account);
        modelAndView.addObject("friends", friends);
        modelAndView.addObject("sendRequests", sendRequests);
        modelAndView.addObject("acceptRequests", acceptRequests);
        return modelAndView;
    }

    @RequestMapping(value = "/friendRequest", method = RequestMethod.GET)
    public String sendFriendRequest(@RequestParam int id, HttpSession httpSession) {
        logger.info("Friend(id = {}) requests", id);
        Account account = (Account) httpSession.getAttribute("account");
        accountService.sendFriendRequest(account.getId(), id);
        return "redirect:/account?id=" + id;
    }

    @RequestMapping(value = "/addFriend", method = RequestMethod.GET)
    public String addFriend(@RequestParam int id, @RequestParam boolean isPage, HttpSession httpSession) {
        logger.info("Add friend(id = {})", id);
        Account account = (Account) httpSession.getAttribute("account");
        accountService.acceptFriendRequest(account.getId(), id);
        if (!isPage) {
            return "redirect:/friends?id=" + account.getId();
        }
        return "redirect:/account?id=" + id;
    }

    @RequestMapping(value = "/deleteFriend", method = RequestMethod.GET)
    public String deleteFriend(@RequestParam int id, @RequestParam boolean isPage, HttpSession httpSession) {
        logger.info("Delete friend(id = {})", id);
        Account account = (Account) httpSession.getAttribute("account");
        accountService.deleteFriendship(account.getId(), id);
        if (!isPage) {
            return "redirect:/friends?id=" + account.getId();
        }
        return "redirect:/account?id=" + id;
    }
}
