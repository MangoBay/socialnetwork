package com.getjavajob.training.web1902.koryukinr.webapp.controllers;

import com.getjavajob.training.web1902.koryukinr.common.*;
import com.getjavajob.training.web1902.koryukinr.service.AccountService;
import com.getjavajob.training.web1902.koryukinr.service.GroupService;
import com.getjavajob.training.web1902.koryukinr.service.MessageService;
import com.thoughtworks.xstream.XStream;
import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.*;

import static com.getjavajob.training.web1902.koryukinr.webapp.utils.CommonUtils.fillInitBinder;
import static com.getjavajob.training.web1902.koryukinr.webapp.utils.CommonUtils.getEncodePassword;
import static com.getjavajob.training.web1902.koryukinr.webapp.utils.CommonUtils.setContentType;
import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.MediaType.APPLICATION_XML;

@Controller
public class AccountController {
    private static final Logger logger = LoggerFactory.getLogger(AccountController.class);

    @Autowired
    private AccountService accountService;
    @Autowired
    private GroupService groupService;
    @Autowired
    private MessageService messageService;

    @InitBinder
    protected void initBinder(WebDataBinder binder) {
        logger.trace("Init common properties");
        fillInitBinder(binder);
    }

    @RequestMapping(value = "/getAllEntities")
    @ResponseBody
    public List<Object> getEntities(@RequestParam("filter") String filter) {
        logger.info("Get all groups and accounts by filter = {}", filter);
        List<Account> accounts = accountService.getAllAccounts();
        List<Group> groups = groupService.getAll();

        CollectionUtils.filter(accounts, account ->
                account.getFirstName().contains(filter) || account.getLastName().contains(filter));
        CollectionUtils.filter(groups, group -> group.getName().contains(filter));

        List<Object> entities = new ArrayList<>();
        entities.addAll(accounts);
        entities.addAll(groups);
        return entities;
    }

    @RequestMapping(value = "/account", method = RequestMethod.GET)
    public ModelAndView account(@RequestParam("id") int id, HttpSession httpSession) {
        logger.info("Data generation for account with id = {}", id);
        Account account = accountService.getById(id);
        Account accountFromSession = (Account) httpSession.getAttribute("account");
        ModelAndView modelAndView = new ModelAndView("account");
        modelAndView.addObject("account", accountService.getById(id));
        modelAndView.addObject("isAdmin", account.isAdmin());
        modelAndView.addObject("isFriend", accountService.checkFriendship(accountFromSession, account));
        modelAndView.addObject("isSubscribe", accountService.checkRequestToMe(accountFromSession, account));
        modelAndView.addObject("isWaitResponse", accountService.checkResponseToMe(accountFromSession, account));

        modelAndView.addObject("messageWall", messageService.findAllMessagesByIdTo(id, MessageType.ACCOUNT_WALL));
        return modelAndView;
    }

    @RequestMapping(value = "/accountGroups", method = RequestMethod.GET)
    public ModelAndView accountGroups(HttpSession httpSession) {
        logger.info("Retrieving account group information");
        Account sessionAccount = (Account) httpSession.getAttribute("account");
        ModelAndView modelAndView = new ModelAndView("accountGroups");
        modelAndView.addObject("groups", groupService.findGroupByOwnerId(sessionAccount.getId()));
        modelAndView.addObject("groupsWithMe", groupService.getGroupsWithMe(sessionAccount.getId()));
        modelAndView.addObject("requestGroupsByMe", groupService.sendRequestGroupsByMe(sessionAccount.getId()));
        return modelAndView;
    }

    @RequestMapping(value = "/register", method = RequestMethod.GET)
    public ModelAndView register() {
        logger.info("Create new account and redirect on view for fill data");
        return new ModelAndView("register", "newAccount", new Account());
    }

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String doRegister(@ModelAttribute Account account, HttpServletRequest request, HttpSession session) throws ServletException {
        logger.info("Registration new account");
        String email = account.getEmail();
        String password = account.getPassword();

        account.setPassword(getEncodePassword(password));
        accountService.createAccount(account);
        Account added = accountService.findByMailAndPass(email, account.getPassword());
        request.login(email, password);
        session.setAttribute("account", account);
        return "redirect: /account?id=" + added.getId();
    }

    @RequestMapping(value = "/editAccount", method = RequestMethod.GET)
    public ModelAndView editAccount(@RequestParam("id") int id,
                                    @SessionAttribute("account") Account accountFromSession) {
        logger.info("Redirect account(id = {}) for save data", id);
        Account account = accountService.getById(id);
        if (account == accountFromSession || (accountFromSession.isAdmin() && !account.isAdmin())) {
            return new ModelAndView("editAccount", "account", account);
        }
        return new ModelAndView("editAccount", "account", accountFromSession);
    }

    @RequestMapping(value = "/updateAccount", method = RequestMethod.POST)
    public String doUpdateAccount(@ModelAttribute Account account,
                                  @RequestParam(value = "phones", required = false) String[] phones,
                                  HttpSession httpSession) {
        logger.info("Update account");
        Account accountFromSession = (Account) httpSession.getAttribute("account");
        Account accountBeforeEdit = accountService.getById(account.getId());
        account.setAdmin(accountBeforeEdit.isAdmin());
        account.setRegistrationDate(accountBeforeEdit.getRegistrationDate());
        account.setPassword(getEncodePassword(account.getPassword()));
        if (account.getPhoto().length == 0 && accountBeforeEdit.getPhoto().length != 0) {
            account.setPhoto(accountBeforeEdit.getPhoto());
        }
        accountService.addPhones(account, phones);
        accountService.updateAccount(account);
        Account updated = accountService.getById(account.getId());
        if (updated.getId() == accountFromSession.getId()) {
            httpSession.setAttribute("account", updated);
        }
        return "redirect: /account?id=" + updated.getId();
    }

    @RequestMapping(value = "/makeAdmin", method = RequestMethod.GET)
    public String makeAdmin(@RequestParam int id) {
        logger.info("Make an account(id = {}) administrator", id);
        Account account = accountService.getById(id);
        account.setAdmin(true);
        accountService.updateAccount(account);
        return "redirect: /account?id=" + account.getId();
    }

    @RequestMapping(value = "/deleteAccount", method = RequestMethod.GET)
    public String deleteAccount(@RequestParam int id, @RequestParam boolean isMine,
                                @SessionAttribute("account") Account accountFromSession) {
        logger.info("Delete account = {}", id);
        Account account = accountService.getById(id);
        accountService.deleteAccount(account);
        if (isMine) {
            return "redirect: /logout";
        }
        return "redirect: /account?id=" + accountFromSession.getId();
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String doLogin(@RequestParam(value = "error", required = false) String error,
                          @RequestParam(value = "logout", required = false) String logout,
                          @SessionAttribute(required = false, name = "account") Account account, Model model) {
        logger.info("Login account");
        if (error != null) {
            model.addAttribute("error", "Неправильный логин или пароль");
        } else if (logout != null) {
            model.addAttribute("logout", "Выход прошел успешно");
        } else {
            if (account != null) {
                return "redirect: /account?id=" + account.getId();
            }
        }
        return "login";
    }

    @RequestMapping(value = "/accountPrintImage", method = RequestMethod.GET)
    public void accountPrintImage(HttpServletResponse response, @RequestParam("id") int id) throws IOException {
        logger.info("Print image for account(id = {})", id);
        Account account = accountService.getById(id);
        byte[] image = account.getPhoto();
        setContentType(response, image);
    }

    @RequestMapping(value = "/searchAccountsByPageAjax", method = RequestMethod.GET)
    @ResponseBody
    public List<Account> searchAccountsByPageAjax(@RequestParam String search, @RequestParam int page) {
        logger.info("Search = {} accounts by page = {} with ajax", search, page);
        List<Account> foundAccounts = accountService.getFoundAccounts(search);
        return accountService.calculateAccountsByPage(page, foundAccounts);
    }

    @RequestMapping(value = "/searchLine", method = RequestMethod.GET)
    public ModelAndView doSearch(@RequestParam String search) {
        logger.info("Search all entities (accounts and groups) by searchLine = {}", search);
        ModelAndView modelAndView = new ModelAndView("foundAll");
        String searchByLowerCase = search.toLowerCase();
        List<Account> foundAccounts = accountService.getFoundAccounts(searchByLowerCase);
        List<Group> foundGroups = groupService.getFoundGroups(searchByLowerCase);
        modelAndView.addObject("foundAccounts", foundAccounts);
        modelAndView.addObject("foundGroups", foundGroups);
        modelAndView.addObject("search", search);
        return modelAndView;
    }

    @RequestMapping(value = "/chat", method = RequestMethod.GET)
    public ModelAndView chat(@SessionAttribute("account") Account account) {
        return new ModelAndView("chat", "friends", accountService.getFriends(account));
    }

    /**
     * Необходимые поля для заполнения -
     * birthDate, firstName, lastName, email, password
     */
    @RequestMapping(value = "/registerAccountFromXML", method = RequestMethod.POST)
    public String registerAccountFromXML(@RequestParam(value = "file", required = false) MultipartFile file,
                                         HttpServletRequest request) throws IOException, ServletException {
        logger.info("Registration account from xml file");
        if (!file.isEmpty()) {
            logger.debug("Read account data from xml file for registration");
            XStream xstream = new XStream();
            xstream.processAnnotations(Account.class);
            setXStreamDetails(xstream);

            Account xmlAccount = (Account) xstream.fromXML(new String(file.getBytes()));
            String simplePassword = xmlAccount.getPassword();
            xmlAccount.setPassword(getEncodePassword(simplePassword));
            accountService.createAccount(xmlAccount);

            Account added = accountService.findByMail(xmlAccount.getEmail());
            request.login(added.getEmail(), simplePassword);
            request.getSession().setAttribute("account", added);
            return "redirect: /account?id=" + added.getId();
        }
        return "redirect: /register";
    }

    @RequestMapping(value = "/toXML", method = RequestMethod.GET)
    public void convertAccountToXML(@RequestParam int id, HttpServletResponse response,
                                    @SessionAttribute("account") Account accountFromSession) {
        logger.info("Convert account(id = {}) to xml file", id);
        Account account = accountService.getById(id);
        if (account != null) {
            XStream xStream = new XStream();
            xStream.processAnnotations(Account.class);
            if (account == accountFromSession || (accountFromSession.isAdmin() && !account.isAdmin())) {
                convertAccountToXMLDetails(response, account, xStream);
            } else {
                convertAccountToXMLDetails(response, accountFromSession, xStream);
            }
        }
    }

    @RequestMapping(value = "/editAccountFromXML", method = RequestMethod.POST)
    public String editAccountFromXML(@RequestParam(value = "file") MultipartFile file,
                                     @RequestParam int id, HttpSession httpSession,
                                     @SessionAttribute("account") Account accountFromSession) throws IOException {
        logger.info("Update account(id = {}) data from xml file", id);
        Account account = accountService.getById(id);
        if (!file.isEmpty()) {
            logger.debug("Read account(id = {}) data from xml file for save", id);
            XStream xstream = new XStream();
            xstream.processAnnotations(Account.class);
            setXStreamDetails(xstream);
            Account xmlAccount = (Account) xstream.fromXML(new String(file.getBytes()));
            xmlAccount.setPassword(getEncodePassword(xmlAccount.getPassword()));
            accountService.setPropertiesAccount(account, xmlAccount);
        }
        accountService.updateAccount(account);
        Account updated = accountService.getById(account.getId());
        if (updated.getId() == accountFromSession.getId()) {
            httpSession.setAttribute("account", updated);
        }
        return "redirect: /account?id=" + updated.getId();
    }

    private void convertAccountToXMLDetails(HttpServletResponse response, Account account, XStream xStream) {
        response.addHeader(CONTENT_DISPOSITION, "attachment;filename=account_" + account.getId() + ".xml");
        response.setContentType(String.valueOf(APPLICATION_XML));
        try {
            logger.debug("Write account(id = {}) data on xml file", account.getId());
            response.getOutputStream().write(xStream.toXML(account).getBytes());
            response.flushBuffer();
        } catch (IOException e) {
            logger.error("Account(id = {}) data conversion failed", account.getId());
            response.setStatus(SC_BAD_REQUEST);
        }
    }

    private void setXStreamDetails(XStream xstream) {
        Class<?>[] classes = new Class[]{Account.class};
        XStream.setupDefaultSecurity(xstream);
        xstream.allowTypes(classes);
    }
}
