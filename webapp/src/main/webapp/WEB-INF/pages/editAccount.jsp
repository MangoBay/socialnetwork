<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fort" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>Редактирование аккаунта</title>
    <link rel="stylesheet" href="<c:url value="/styles/registerPage.css"/>">

</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <form:form action="/updateAccount" method="post" enctype="multipart/form-data"
                       modelAttribute="account" id="formContentOne">
                <h3>Редактирование при помощи ввода данных</h3>
                <form:input type="hidden" path="id" value="${account.id}"/>
                <div class="form-group">
                    <label for="inputFirstName">Имя</label>
                    <form:input path="firstName" required="required" class="form-control" id="inputFirstName"
                                placeholder="Введите имя"/>
                </div>
                <div class="form-group">
                    <label for="inputLastName">Фамилия</label>
                    <form:input path="lastName" required="required" class="form-control" id="inputLastName"
                                placeholder="Введите фамилию"/>
                </div>
                <div class="form-group">
                    <label for="inputMiddleName">Отчество</label>
                    <form:input path="middleName" class="form-control" id="inputMiddleName"/>
                </div>
                <div class="form-group">
                    <label for="inputBirthDate">Дата рождения</label>
                    <form:input type="date" path="birthDate" required="required" class="form-control"
                                id="inputBirthDate" value="${account.birthDate}"/>
                </div>
                <div class="form-group">
                    <label for="inputEmail">Почта</label>
                    <form:input type="email" path="email" required="required" class="form-control" id="inputEmail"
                                aria-describedby="emailHelp"
                                placeholder="Введите почту"/>
                    <small id="emailHelp" class="form-text text-muted">Данную почту необходимо будет использовать в виде
                        логина при входе
                    </small>
                </div>
                <div class="form-group">
                    <label for="inputPassword">Пароль</label>
                    <form:password path="password" required="required" class="form-control" id="inputPassword"
                                   placeholder="Введите пароль"/>
                </div>
                <c:if test="${requestScope.account.phones.isEmpty() == false}">
                    <div class="form-group">
                        <label for="newPhoneAdd">Текущие телефоны</label>
                        <ul>
                            <c:forEach var="phones" items="${requestScope.account.phones}">
                                <li>
                                    <input type="text" placeholder="Номер телефона" value="${phones.number}"
                                           readonly id="newPhoneAdd" class="form-control"><br>
                                    <input type="hidden" name="phones" value="${phones.number}">
                                    <button type="button" class="btn btn-outline-danger removePhoneButton">Удалить
                                    </button>
                                </li>
                            </c:forEach>
                        </ul>
                    </div>
                </c:if>
                <div class="form-group">
                    <label for="phoneNumber">Добавленные телефоны</label>
                    <ol></ol>
                    <input type="text" placeholder="х (ххх) ххх-хххх" id="phoneNumber" class="form-control"><br>
                    <button type="button" class="btn btn-outline-primary " id="addPhoneButton">Добавить</button>
                </div>
                <div class="form-group">
                    <label for="inputHomeAddress">Домашний адрес</label>
                    <form:input path="homeAddress" class="form-control" id="inputHomeAddress"/>
                </div>
                <div class="form-group">
                    <label for="inputWorkAddress">Рабочий адрес</label>
                    <form:input path="workAddress" class="form-control" id="inputWorkAddress"/>
                </div>
                <div class="form-group">
                    <label for="inputSkype">Скайп</label>
                    <form:input path="skype" class="form-control" id="inputSkype"/>
                </div>
                <div class="form-group">
                    <label for="inputAdditionalInfo">Дополнительная информация</label>
                    <form:textarea path="additionalInfo" class="form-control" id="inputAdditionalInfo" rows="3"/>
                </div>
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <form:input type="file" path="photo" class="custom-file-input" id="inputFile1"/>
                        <label class="custom-file-label" for="inputFile1" data-browse="Выбрать">Выберите фотографию</label>
                    </div>
                </div>
                <c:if test="${(sessionScope.account.isAdmin() == true && account.admin != true) ||
                 (sessionScope.account.id == requestScope.account.id)}">
                    <a href="#" class="btn btn-danger btn-lg btn-block" role="button" aria-pressed="true"
                       data-toggle="modal" data-target="#exampleModal2">Удалить аккаунт</a>
                </c:if>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal"
                        id="buttonFormOne">Сохранить
                </button>
                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Сохранение изменений</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Сохранить изменения?
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-secondary">Сохранить
                                </button>
                                <a href="#" class="btn btn-primary" data-dismiss="modal">Отменить</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel2"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel2">Удаление аккаунта</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Вы уверены что хотите удалить аккаунт?
                            </div>
                            <div class="modal-footer">
                                <c:if test="${sessionScope.account.isAdmin() == true && account.admin != true}">
                                    <a href="<c:url value="/deleteAccount?id=${account.id}&isMine=0"/>"
                                       class="btn btn-danger">Удалить</a>
                                </c:if>
                                <c:if test="${sessionScope.account.id == requestScope.account.id}">
                                    <a href="<c:url value="/deleteAccount?id=${account.id}&isMine=1"/>"
                                       class="btn btn-danger">Удалить</a>
                                </c:if>
                                <a href="#" class="btn btn-primary" data-dismiss="modal">Отменить</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>
        </div>
        <div class="col-sm">
            <form action="<c:url value="/editAccountFromXML?id=${account.id}"/>" method="post"
                  enctype="multipart/form-data" id="formContentTwo">
                <h3>Редактирование при помощи XML</h3>
                <h3>Пример XML - файла</h3>
                <div class="form-group">
                    <code>
                        &ltAccount&gt <br>
                        &nbsp&nbsp&ltbirthDate&gt1980-07-15&lt/birthDate&gt <br>
                        &nbsp&nbsp&ltfirstName&gtМихаил&lt/firstName&gt <br>
                        &nbsp&nbsp&ltlastName&gtПетов&lt/lastName&gt <br>
                        &nbsp&nbsp&ltemail&gtpetrov@mail.ru&lt/email&gt <br>
                        &nbsp&nbsp&ltpassword&gt123&lt/password&gt <br>
                        &lt/Account&gt
                    </code>
                </div>
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputFile" name="file">
                        <label class="custom-file-label" for="inputFile" data-browse="Выбрать">Выберите XML - файл</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="buttonFormTwo">Сохранить</button>
            </form>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="<c:url value="/scripts/phoneEditor.js"/>"></script>
<script src="<c:url value="/scripts/uploadFile.js"/>"></script>
</body>
</html>
