<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Регистрация</title>
    <link rel="stylesheet" href="<c:url value="/styles/registerPage.css"/>">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            <form:form action="/register" method="post" id="formContentOne" enctype="multipart/form-data"
                       modelAttribute="newAccount">
                <h1>Регистрация при помощи ввода данных</h1>
                <div class="form-group">
                    <label for="inputFirstName">Имя</label>
                    <form:input path="firstName" required="required" class="form-control" id="inputFirstName"
                                placeholder="Введите имя"/>
                </div>
                <div class="form-group">
                    <label for="inputLastName">Фамилия</label>
                    <form:input path="lastName" required="required" class="form-control" id="inputLastName"
                                placeholder="Введите фамилию"/>
                </div>
                <div class="form-group">
                    <label for="inputMiddleName">Отчество</label>
                    <form:input path="middleName" class="form-control" id="inputMiddleName"/>
                </div>
                <div class="form-group">
                    <label for="inputBirthDate">Дата рождения</label>
                    <form:input type="date" path="birthDate" required="required" class="form-control"
                                id="inputBirthDate"/>
                </div>
                <div class="form-group">
                    <label for="inputEmail">Почта</label>
                    <form:input type="email" path="email" required="required" class="form-control" id="inputEmail"
                                aria-describedby="emailHelp"
                                placeholder="Введите почту"/>
                    <small id="emailHelp" class="form-text text-muted">Данную почту необходимо будет использовать в виде
                        логина при входе
                    </small>
                </div>
                <div class="form-group">
                    <label for="inputPassword">Пароль</label>
                    <form:password path="password" required="required" class="form-control" id="inputPassword"
                                   placeholder="Введите пароль"/>
                </div>
                <div class="form-group">
                    <label for="inputHomeAddress">Домашний адрес</label>
                    <form:input path="homeAddress" class="form-control" id="inputHomeAddress"/>
                </div>
                <div class="form-group">
                    <label for="inputWorkAddress">Рабочий адрес</label>
                    <form:input path="workAddress" class="form-control" id="inputWorkAddress"/>
                </div>
                <div class="form-group">
                    <label for="inputSkype">Скайп</label>
                    <form:input path="skype" class="form-control" id="inputSkype"/>
                </div>
                <div class="form-group">
                    <label for="inputAdditionalInfo">Дополнительная информация</label>
                    <form:textarea path="additionalInfo" class="form-control" id="inputAdditionalInfo" rows="3"/>
                </div>
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <form:input type="file" path="photo" class="custom-file-input" id="inputFile1"/>
                        <label class="custom-file-label" for="inputFile1" data-browse="Выбрать">Выберите
                            фотографию</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="buttonFormOne">Зарегистрироваться</button>
                <div class="form-group">
                    <label for="hrefLogin">Уже есть аккаунт?</label>
                    <a href="<c:url value="${pageContext.request.contextPath}/login"/>" id="hrefLogin">Войти</a>
                </div>
            </form:form>
        </div>
        <div class="col-sm">
            <form action="<c:url value="/registerAccountFromXML"/>" id="formContentTwo" method="POST"
                  enctype="multipart/form-data">
                <h1>Регистрация при помощи XML</h1>
                <h3>Пример XML - файла</h3>
                <div class="form-group">
                    <code>
                        &ltAccount&gt <br>
                        &nbsp&nbsp&ltbirthDate&gt1980-07-15&lt/birthDate&gt <br>
                        &nbsp&nbsp&ltfirstName&gtМихаил&lt/firstName&gt <br>
                        &nbsp&nbsp&ltlastName&gtПетов&lt/lastName&gt <br>
                        &nbsp&nbsp&ltemail&gtpetrov@mail.ru&lt/email&gt <br>
                        &nbsp&nbsp&ltpassword&gt123&lt/password&gt <br>
                        &lt/Account&gt
                    </code>
                </div>
                <div class="input-group mb-3">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="inputFile" name="file">
                        <label class="custom-file-label" for="inputFile" data-browse="Выбрать">Выберите XML -
                            файл</label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary" id="buttonFormTwo">Зарегистрироваться</button>
            </form>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-1.8.3.js"></script>
<script src="<c:url value="/scripts/uploadFile.js"/>"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
