<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>Найденные совпадения</title>
    <link rel="stylesheet" href="<c:url value="/styles/foundGroupsAndAccounts.css"/>">
    <link rel="stylesheet" href="<c:url value="/styles/pagination.css"/>">
</head>
<body>
<div class="text-center">
    <h4>По данному запросу - <span class="searchLabel">${search}</span> были найдены:</h4>
    <button class="hrefToFirstAccountPage btn btn-primary btn-lg" value="1">Аккаунты(<span
            class="countAccount">${foundAccounts.size()}</span>)
    </button>
    <button class="hrefToFirstGroupPage btn btn-secondary btn-lg" value="1">Группы(<span
            class="countGroup">${foundGroups.size()}</span>)
    </button>
    <div class="resultAccounts"></div>
    <div class="resultGroups"></div>
</div>

<script src="<c:url value="/scripts/pagination.js"/>"></script>

</body>
</html>
