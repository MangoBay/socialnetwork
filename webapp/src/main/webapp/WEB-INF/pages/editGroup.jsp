<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>Редактирование группы</title>
</head>
<body>
<br>
<div class="container" style="background: #f2f3f4;">
    <div class="row">
        <div class="col-sm">
            <form:form action="/updateGroup?id=${group.id}" method="post" enctype="multipart/form-data" id="formContent"
                       modelAttribute="group">
                <div class="form-group col-md-4 mx-auto">
                    <label for="exampleInputEmail1" id="label1">Наименование группы</label>
                    <form:input path="name" required="required" placeholder="Имя группы" class="form-control"
                                id="exampleInputEmail1"/>
                </div>
                <div class="form-group col-md-4 mx-auto">
                    <label for="inputAdditionalInfo" id="label2">Описание группы</label>
                    <form:textarea path="description" required="required" rows="5" class="form-control"
                                   id="inputAdditionalInfo"/>
                </div>
                <div class="input-group col-md-4 mx-auto">
                    <div class="custom-file">
                        <form:input type="file" path="photo" class="custom-file-input" id="inputFile1"/>
                        <label class="custom-file-label" for="inputFile1" data-browse="Выбрать">Фотография
                            группы</label>
                    </div>
                </div>
                <div class="input-group col-md-4 mx-auto">
                    <button type="button" class="btn btn-primary " id="buttonCenter" data-toggle="modal"
                            data-target="#exampleModal">
                        Сохранить
                    </button>
                </div>


                <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel">Сохранение изменений</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                Вы уверены что хотите сохранить изменения?
                            </div>
                            <div class="modal-footer">
                                <button type="submit" class="btn btn-secondary">Сохранить</button>
                                <a href="#" class="btn btn-primary" data-dismiss="modal">Отменить</a>
                            </div>
                        </div>
                    </div>
                </div>
                <c:if test="${sessionScope.account.id == group.owner}">
                    <div class="input-group col-md-4 mx-auto">
                        <a class="btn btn-danger" href="#" role="button" id="buttonDelete"
                           data-toggle="modal" data-target="#exampleModal2">Удалить группу</a>
                    </div>
                    <div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel2" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel2">Удаление группы</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    Вы уверены что хотите удалить группу?
                                </div>
                                <div class="modal-footer">
                                    <a href="<c:url value="/deleteGroup?id=${group.id}"/>" class="btn btn-danger">Удалить</a>
                                    <a href="#" class="btn btn-primary" data-dismiss="modal">Отменить</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </c:if>
            </form:form>
        </div>
    </div>
</div>
<script src="<c:url value="/scripts/uploadFile.js"/>"></script>
</body>
</html>
