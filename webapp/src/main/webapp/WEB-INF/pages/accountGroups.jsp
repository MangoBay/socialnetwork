<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>Группы</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            Мои группы:
            <form>
                <div class="form-group">
                    <c:forEach var="group" items="${requestScope.groups}">
                        <a href="<c:url value="/group?id=${group.id}"/>">${group.name}</a><br>
                    </c:forEach>
                </div>
                <p><a href="<c:url value="/createGroup"/>">
                    <input type="button" class="btn btn-primary" value="Создать группу"></a></p>
            </form>
        </div>
        <div class="col-sm">
            Группы в которых я состою:
            <form>
                <div class="form-group">
                    <c:forEach var="group" items="${groupsWithMe}">
                        <a href="<c:url value="/group?id=${group.id}"/>">${group.name}</a><br>
                    </c:forEach>
                </div>
            </form>
        </div>
        <div class="col-sm">
            Отправленные заявки:
            <form>
                <div class="form-group">
                    <c:forEach var="group" items="${requestGroupsByMe}">
                        <a href="<c:url value="/group?id=${group.id}"/>">${group.name}</a><br>
                    </c:forEach>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
