<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>${group.name}</title>
    <link rel="stylesheet" href="<c:url value="/styles/groupDescription.css"/>">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<br>
<div class="container">
    <div class="row">
        <div class="col-4">
            <form>
                <c:choose>
                    <c:when test="${fn:length(requestScope.group.getPhoto()) != 0}">
                        <img class="img-fluid img-thumbnail"
                             src="${pageContext.request.contextPath}/groupPrintImage?groupId=${requestScope.group.id}"
                             width="450"
                             height="350"/>
                    </c:when>
                    <c:when test="${fn:length(requestScope.group.getPhoto()) == 0}">
                        <img class="img-fluid img-thumbnail" src="<c:url value="/images/anonGroup.jpg"/>" width="350"
                             height="350">
                    </c:when>
                </c:choose>
            </form>
        </div>
        <div class="col-sm">
            <form>
                <h3>ИНФОМРАЦИЯ О ГРУППЕ</h3>
                Имя: ${group.name} <br>
                Описание: ${group.description} <br>
                Дата регистрации: ${group.createdDate} <br>
                <c:if test="${isNewMembership == true}">
                    <a class="btn btn-primary" href="<c:url value="/requestGroupMembership?id=${group.id}"/>"
                       role="button">Отправить заявку</a>
                </c:if>
                <c:forEach var="gra" items="${groupMembershipRequestAccounts}">
                    <c:if test="${gra.id == sessionScope.account.id}">
                        <a class="btn btn-primary"
                           href="<c:url value="/deleteGroupMembership?groupId=${requestScope.group.id}&accountId=${gra.id}"/>"
                           role="button">Отозвать заявку</a>
                    </c:if>
                </c:forEach>
                <c:forEach var="gaa" items="${groupMembershipUserAndAdminAccounts}">
                    <c:if test="${gaa.id == sessionScope.account.id && gaa.id != group.owner}">
                        <a class="btn btn-primary"
                           href="<c:url value="/deleteGroupMembership?groupId=${requestScope.group.id}&accountId=${gaa.id}"/>"
                           role="button">Выйти из группы</a>
                    </c:if>
                </c:forEach>
                <c:if test="${isAdmin == true}">
                    <a class="btn btn-primary" href="<c:url value="/editGroup?id=${requestScope.group.id}"/>"
                       role="button">Редактировать</a>
                </c:if>
            </form>
        </div>
    </div>

    <c:if test="${isAdmin == true}">
    <div class="row">
        <div class="col-sm">
            <a class="btn btn-primary" data-toggle="collapse" href="#multiCollapseExample1" role="button"
               aria-expanded="false" aria-controls="multiCollapseExample1">Заявки</a>
            <button class="btn btn-primary" type="button" data-toggle="collapse"
                    data-target="#multiCollapseExample2" aria-expanded="false"
                    aria-controls="multiCollapseExample2">Пользователи
            </button>
            <button class="btn btn-primary" type="button" data-toggle="collapse" data-target=".multi-collapse"
                    aria-expanded="false" aria-controls="multiCollapseExample1 multiCollapseExample2">Все вместе
            </button>
            <div class="row">
                <div class="col">
                    <div class="collapse multi-collapse" id="multiCollapseExample1">
                        <div class="card card-body">
                            <form>
                                <c:forEach var="gra" items="${groupMembershipRequestAccounts}">
                                    <a href="<c:url value="/account?id=${gra.id}"/>"><b>${gra.firstName} ${gra.lastName}</b></a>
                                    <a href="<c:url value="/acceptGroupMembershipRequest?groupId=${requestScope.group.id}&accountId=${gra.id}"/>">Принять
                                        заявку</a>
                                    <a href="<c:url value="/deleteGroupMembership?groupId=${requestScope.group.id}&accountId=${gra.id}"/>">Отклонить
                                        заявку</a>
                                    <hr>
                                </c:forEach>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <div class="collapse multi-collapse" id="multiCollapseExample2">
                        <div class="card card-body">
                            <form>
                                <c:forEach var="gua" items="${groupMembershipUserAccounts}">
                                    <a href="<c:url value="/account?id=${gua.id}"/>"><b>${gua.firstName} ${gua.lastName}</b></a>
                                    <a href="<c:url value="/deleteGroupMembership?groupId=${requestScope.group.id}&accountId=${gua.id}"/>">Исключить
                                        из группы</a>
                                    <a href="<c:url value="/makeAdminByGroupAndAccount?groupId=${requestScope.group.id}&accountId=${gua.id}"/>">Сделать
                                        админом</a>
                                    <hr>
                                </c:forEach>
                                <c:choose>
                                    <c:when test="${isOwner == true}">
                                        <c:forEach var="gaa" items="${groupMembershipAdminAccounts}">
                                            <c:choose>
                                                <c:when test="${gaa.id == group.owner}">
                                                    <h6>Создатель группы -
                                                        <a href="<c:url value="/account?id=${gaa.id}"/>"><b>${gaa.firstName} ${gaa.lastName}</b></a>
                                                    </h6>
                                                </c:when>
                                                <c:when test="${gaa.id != group.owner}">
                                                    <a href="<c:url value="/account?id=${gaa.id}"/>"><b>${gaa.firstName} ${gaa.lastName}</b></a>
                                                    <a href="<c:url value="/deleteFromAdmin?groupId=${requestScope.group.id}&accountId=${gaa.id}"/>">Удалить
                                                        администратора</a><br>
                                                </c:when>
                                            </c:choose>
                                            <hr>
                                        </c:forEach>
                                    </c:when>
                                    <c:when test="${isOwner != true}">
                                        <c:forEach var="gaa" items="${groupMembershipAdminAccounts}">
                                            <c:choose>
                                                <c:when test="${gaa.id == group.owner}">
                                                    <h6>Создатель группы -
                                                        <a href="<c:url value="/account?id=${gaa.id}"/>"><b>${gaa.firstName} ${gaa.lastName}</b></a>
                                                    </h6>
                                                    <hr>
                                                </c:when>
                                                <c:when test="${gaa.id != group.owner}">
                                                    <c:if test="${gaa.id != group.owner}">
                                                        <a href="<c:url value="/account?id=${gaa.id}"/>"><b>${gaa.firstName} ${gaa.lastName}</b></a><br>
                                                    </c:if>
                                                    <hr>
                                                </c:when>
                                            </c:choose>
                                        </c:forEach>
                                    </c:when>
                                </c:choose>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </c:if>
    <c:if test="${isUser == true}">
    <br>
    <div class="row">
        <div class="col-sm">
            <div class="form-group">
                <form action="<c:url value="/sendMessageToWall"/>" method="post">
                    <input type="hidden" name="idToGroupWall" value="${group.id}"/>

                    <div class="input-group mb-3">
                        <textarea class="form-control" aria-label="With textarea" aria-describedby="button-addon"
                                  id="inputMessage" name="message"
                                  placeholder="Отправте сообщение на стену, чтобы все пользователи группы видели его"></textarea>
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon">Отправить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm">
            <c:forEach var="message" items="${messageWall}">
                <div class="likeForm">
                    <c:choose>
                        <c:when test="${fn:length(message.accountFrom.photo) != 0}">
                            <img class="rounded-circle" width="30" height="30"
                                 src="${pageContext.request.contextPath}/accountPrintImage?id=${message.accountFrom.id}"/>
                        </c:when>
                        <c:when test="${fn:length(message.accountFrom.photo) == 0}">
                            <img class="rounded-circle" width="30" height="30"
                                 src="<c:url value="/images/anonPerson.png"/>">
                        </c:when>
                    </c:choose>
                    <a href="<c:url value="/account?id=${message.accountFrom.id}"/>">${message.accountFrom.firstName} ${message.accountFrom.lastName}</a><br>
                    <small class="text-muted">${fn:replace(message.localDate, 'T', ' ')}</small>
                    <div class="groupWallMessage">${message.message}</div>
                    <c:if test="${sessionScope.account.id == message.accountFrom.id || isAdmin == true}">
                        <form action="<c:url value="/removeMessageFromWall"/>" method="post">
                            <input type="hidden" name="id" value="${message.id}">
                            <input type="hidden" name="isDelete" value="1">
                            <button class="btn float-right remove-btn" type="submit">
                                <i class="fa fa-trash fa-2x"></i>
                            </button>
                        </form>
                    </c:if>
                </div>
                <br>
            </c:forEach>
        </div>
    </div>
    </c:if>
</body>
</html>

