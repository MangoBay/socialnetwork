<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>Создание группы</title>
</head>
<body>
<br>
<div class="container" style="background: #f2f3f4;">
    <div class="row">
        <div class="col-sm">
            <form:form action="/createGroup" method="post" modelAttribute="group" enctype="multipart/form-data" id="formContent">
                <div class="form-group col-md-4 mx-auto">
                    <label for="exampleInputEmail1" id="label1">Наименование группы</label>
                    <form:input path="name" required="required" placeholder="Имя группы" class="form-control"  id="exampleInputEmail1"/>
                </div>
                <div class="form-group col-md-4 mx-auto">
                    <label for="inputAdditionalInfo" id="label2">Описание группы</label>
                    <form:textarea path="description" required="required" rows="5" class="form-control" id="inputAdditionalInfo"/>
                </div>
                <div class="input-group col-md-4 mx-auto">
                    <div class="custom-file">
                        <form:input type="file" path="photo" class="custom-file-input" id="inputFile1"/>
                        <label class="custom-file-label" for="inputFile1" data-browse="Выбрать">Фотография группы</label>
                    </div>
                </div>
                <div class="input-group col-md-4 mx-auto">
                    <button type="submit" class="btn btn-primary" id="buttonCenter">Создать</button>
                </div>
            </form:form>
        </div>
    </div>
</div>
<script src="<c:url value="/scripts/uploadFile.js"/>"></script>
</body>
</html>
