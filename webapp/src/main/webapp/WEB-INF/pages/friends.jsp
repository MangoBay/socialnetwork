<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>Друзья</title>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-sm">
            Мои друзья
            <form>
                <div class="form-group">
                    <c:forEach var="friend" items="${friends}">
                        <span>
                            <a href="<c:url value="/account?id=${friend.id}"/>">${friend.firstName} ${friend.lastName}</a>
                            <a href="<c:url value="/deleteFriend?id=${friend.id}&isPage=0"/>">Удалить друга</a><br>
                        </span>
                        <hr>
                        <br>
                    </c:forEach>
                </div>
            </form>
        </div>
        <div class="col-sm">
            Полученные заявки
            <form>
                <div class="form-group">
                    <c:forEach var="accept" items="${acceptRequests}">
                        <span>
                            <a href="<c:url value="/account?id=${accept.id}"/>">${accept.firstName} ${accept.lastName}</a>
                            <a href="<c:url value="/addFriend?id=${accept.id}&isPage=0"/>">Подтвердить заявку</a>
                            <a href="<c:url value="/deleteFriend?id=${accept.id}&isPage=0"/>">Отклонить заявку</a>
                        </span>
                        <hr>
                        <br>
                    </c:forEach>
                </div>
            </form>
        </div>
        <div class="col-sm">
            Отпрвленные заявки
            <form>
                <div class="form-group">
                    <c:forEach var="send" items="${sendRequests}">
                        <span>
                            <a href="<c:url value="/account?id=${send.id}"/>">${send.firstName} ${send.lastName}</a>
                            <a href="<c:url value="/deleteFriend?id=${send.id}&isPage=0"/>">Отозвать заявку</a><br>
                            </span>
                        <hr>
                        <br>
                    </c:forEach>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
