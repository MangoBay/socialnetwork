<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>Чат</title>
    <script src="<c:url value="/scripts/messages.js"/>"></script>
    <link href="<c:url value="/styles/chat.css"/>" rel="stylesheet">
</head>
<body>
<input type="hidden" value="${sessionScope.account.id}" id="idFrom">
<div class="container">
    <div class="row">
        <div class="col-4">
            <h2>Друзья</h2>
            <hr>
            <c:forEach var="friend" items="${friends}">
                <c:choose>
                    <c:when test="${fn:length(friend.photo) != 0}">
                        <p>
                            <img class="img-thumbnail"
                                 src="${pageContext.request.contextPath}/accountPrintImage?id=${friend.id}"
                                 width="64" height="64"/>
                            <a href="#" class="friend" id="${friend.id}">${friend.firstName} ${friend.lastName}</a>
                        </p>
                    </c:when>
                    <c:when test="${fn:length(friend.photo) == 0}">
                        <p>
                            <img class="img-thumbnail" src="<c:url value="/images/anonPerson.png"/>" width="64"
                                 height="64">
                            <a href="#" class="friend" id="${friend.id}">${friend.firstName} ${friend.lastName}</a>
                        </p>
                    </c:when>
                </c:choose>

            </c:forEach>
        </div>
        <div class="col-sm">
            <h2>Сообщения</h2>
            <hr>
            <div class="ajaxLoad"></div>
            <div class="messages">
                <div class="messageHistory">
                    <table class="table table-borderless">
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="input-group mb-3 inputGroup">
                <textarea class="form-control" aria-label="With textarea" aria-describedby="button-addon"
                          id="textContent"></textarea>
                <div class="input-group-prepend">
                    <button class="btn btn-outline-secondary" type="button" id="sendMessage">Отправить</button>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/sockjs-client/1.2.0/sockjs.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/stomp.js/2.3.3/stomp.js"></script>
</body>
</html>
