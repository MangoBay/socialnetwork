<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="header.jsp"/>
<html>
<head>
    <title>${sessionScope.account.firstName} ${sessionScope.account.lastName}</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<c:url value="/styles/homePage.css"/>">
</head>
<body>
<br>
<div class="container">
    <div class="row">
        <div class="col-4">
            <form>
                <c:choose>
                    <c:when test="${fn:length(requestScope.account.getPhoto()) != 0}">
                        <img class="img-fluid img-thumbnail"
                             src="${pageContext.request.contextPath}/accountPrintImage?id=${requestScope.account.id}"
                             width="450" height="350"/>
                    </c:when>
                    <c:when test="${fn:length(requestScope.account.getPhoto()) == 0}">
                        <img class="img-fluid img-thumbnail" src="<c:url value="/images/anonPerson.png"/>" width="450"
                             height="350">
                    </c:when>
                </c:choose>
            </form>
        </div>
        <div class="col-sm">
            <form>
                <h5>
                    &nbsp;
                    <img src="https://img.icons8.com/ios/70/000000/name-tag-woman-horizontal.png">
                    &nbsp;
                    ИНФОМРАЦИЯ ОБ АККАУНТЕ
                </h5>
                &nbsp;
                <img src="https://img.icons8.com/metro/30/000000/name.png">
                &nbsp;
                Имя: ${account.firstName} <br>
                &nbsp;
                <img src="https://img.icons8.com/ios-filled/30/000000/name.png">
                &nbsp;
                Фамилия: ${account.lastName} <br>
                &nbsp;
                <img src="https://img.icons8.com/ios/30/000000/email-open.png">
                &nbsp;
                Почта: ${account.email} <br>
                <hr>
                &nbsp;
                <img src="https://img.icons8.com/dotty/30/000000/date-to.png">
                &nbsp;
                Дата регистрации: ${account.registrationDate} <br>
                &nbsp;
                <img src="https://img.icons8.com/wired/30/000000/birthday.png">
                &nbsp;
                День рождения: ${account.birthDate} <br>
                <hr>
                &nbsp;
                <c:choose>
                    <c:when test="${account.admin == true}">
                        <img src="https://img.icons8.com/ios-filled/30/000000/add-administrator.png">
                        &nbsp;
                        Админ: Да <br>

                    </c:when>
                    <c:when test="${account.admin != true}">
                        <img src="https://img.icons8.com/ios-filled/30/000000/remove-administrator.png">
                        &nbsp;
                        Админ: Нет <br>
                    </c:when>
                </c:choose>
                <c:if test="${account.phones.isEmpty() == false}">
                    <hr>
                    &nbsp;
                    <img src="https://img.icons8.com/wired/30/000000/two-smartphones.png">
                    &nbsp;
                    Телефоны:
                    <ol>
                        <c:forEach var="phones" items="${account.phones}">
                            <li style="list-style-type: none">
                                &nbsp;
                                <img src="https://img.icons8.com/wired/30/000000/iphone.png">
                                &nbsp;
                                    ${phones.number}
                            </li>
                        </c:forEach>
                    </ol>
                </c:if>
                <c:if test="${sessionScope.account.id != account.id}">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Дружба
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <c:choose>
                                <c:when test="${requestScope.isWaitResponse}">
                                    <a class="dropdown-item"
                                       href="<c:url value="/deleteFriend?id=${requestScope.account.id}&isPage=1"/>">Отозвать
                                        заявку</a>
                                </c:when>
                                <c:when test="${requestScope.isFriend}">
                                    <a class="dropdown-item"
                                       href="<c:url value="/deleteFriend?id=${requestScope.account.id}&isPage=1"/>">Удалить
                                        друга</a>
                                </c:when>
                                <c:when test="${requestScope.isSubscribe}">
                                    <a class="dropdown-item"
                                       href="<c:url value="/addFriend?id=${requestScope.account.id}&isPage=1"/>">Подтвердить
                                        заявку</a>
                                    <a class="dropdown-item"
                                       href="<c:url value="/deleteFriend?id=${requestScope.account.id}&isPage=1"/>">Отклонить
                                        заявку</a>
                                </c:when>
                                <c:when test="${!requestScope.isSubscribe}">
                                    <a class="dropdown-item"
                                       href="<c:url value="/friendRequest?id=${requestScope.account.id}"/>">Отправить
                                        заявку</a>
                                </c:when>
                            </c:choose>
                        </div>
                    </div>
                </c:if>
                <c:if test="${sessionScope.account.isAdmin() == true && account.admin != true}">
                    <div class="dropdown">
                        <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Модификация аккаунта
                        </button>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item"
                               href="<c:url value="/editAccount?id=${account.id}"/>">Редактировать</a>
                            <a class="dropdown-item" href="<c:url value="/toXML?id=${account.id}"/>">Конвертировать в
                                XML</a>
                            <c:if test="${sessionScope.account.id != account.id && sessionScope.account.isAdmin() == true && isAdmin != true}">
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="<c:url value="/makeAdmin?id=${account.id}"/>">Сделать
                                    админом</a>
                            </c:if>
                        </div>
                    </div>
                </c:if>
            </form>
        </div>
    </div>
    <div class="row">
        <div class="col-4"></div>
        <div class="col-sm">
            <div class="form-group">
                <br>
                <form action="<c:url value="/sendMessageToWall"/>" method="post">
                    <input type="hidden" name="idToAccountWall" value="${account.id}"/>
                    <div class="input-group mb-3">
                        <textarea class="form-control" aria-label="With textarea" aria-describedby="button-addon"
                                  id="inputMessage" name="message"
                                  placeholder="Здесь вы можете поделиться своими мыслями"></textarea>
                        <div class="input-group-prepend">
                            <button class="btn btn-outline-secondary" type="submit" id="button-addon">Сохранить</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-4"></div>
        <div class="col-sm">
            <c:forEach var="message" items="${messageWall}">
                <div class="likeForm">
                    <c:choose>
                        <c:when test="${fn:length(message.accountFrom.photo) != 0}">
                            <img class="rounded-circle" width="30" height="30"
                                 src="${pageContext.request.contextPath}/accountPrintImage?id=${message.accountFrom.id}"/>
                        </c:when>
                        <c:when test="${fn:length(message.accountFrom.photo) == 0}">
                            <img class="rounded-circle" width="30" height="30"
                                 src="<c:url value="/images/anonPerson.png"/>">
                        </c:when>
                    </c:choose>
                    <a href="<c:url value="/account?id=${message.accountFrom.id}"/>">${message.accountFrom.firstName} ${message.accountFrom.lastName}</a><br>
                    <small class="text-muted">${fn:replace(message.localDate, 'T', '  ')}</small>
                    <div class="accountWallMessage">${message.message}</div>
                    <c:if test="${sessionScope.account.id == account.id || sessionScope.account.id == message.accountFrom.id}">
                        <form action="<c:url value="/removeMessageFromWall"/>" method="post">
                            <input type="hidden" name="id" value="${message.id}">
                            <button class="btn float-right remove-btn" type="submit">
                                <i class="fa fa-trash fa-2x"></i>
                            </button>
                        </form>
                    </c:if>
                </div>
                <br>
            </c:forEach>
        </div>
    </div>
</div>
</body>
</html>
