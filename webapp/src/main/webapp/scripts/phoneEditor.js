$(document).ready(function ($) {
    $("#phoneNumber").mask("?8 (999) 999-9999");
    $("#addPhoneButton").click(function () {
        $("span.error").remove();
        var phoneNum = $("#phoneNumber").val();
        var phoneNumOnlyNumbers = phoneNum.replace(/\D/g, "");
        if (phoneNum !== '' && phoneNumOnlyNumbers.length == 11) {
            var elem = $("<li></li>").text(phoneNum);
            var divForSetVal = $("<input type='hidden' name='phones'></div>");
            $(divForSetVal).attr("value", phoneNumOnlyNumbers);
            elem.append(divForSetVal);
            $(elem).append(("<button class = 'btn btn-outline-danger rem'>Удалить</button>"));
            $("ol").append(elem);
            $("#phoneNumber").val("");
            $(".rem").on("click", function () {
                $(this).parent().remove();
            });
        }
        else {
            $("#phoneNumber").after('<span class="error">Неправильный номер телефона - ' + phoneNumOnlyNumbers +
                '(Длина = ' + phoneNumOnlyNumbers.length + '). Длина номера должнать быть равна 11</span>');
        }
    });
    $(".removePhoneButton").click(function () {
        $(this).parent().remove();
    });
});