$(function () {
    $("#entities").autocomplete({
        source: function (request, response) {
            $.ajax({
                url: "/getAllEntities",
                data: {
                    filter: request.term
                },
                method: "GET",
                success: function (data) {
                    response($.map(data, function (entity) {
                        return entity.hasOwnProperty("firstName") ?
                            {label: entity.firstName + ' ' + entity.lastName, value: entity.id, type: "account"} :
                            {label: entity.name, value: entity.id, type: "group"};
                    }));
                }
            });
        },
        minLength: 2,
        select: function (event, ui) {
            var id = ui.item.value;
            location.href = ui.item.type === "account" ? '/account?id=' + id : '/group?id=' + id;
            return false;
        }
    });
});
