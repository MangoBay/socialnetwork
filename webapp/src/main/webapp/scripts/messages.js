$(function () {
    var stompClient = null;
    var subscribeTopic = null;
    var recipientId = null;
    var currentSenderId = $('#idFrom').val();

    var body = $("body");
    var elemTableBody = $('tbody');
    var inputGroup = $('.inputGroup');
    var messageContent = $('#textContent');
    var messageHistory = $('.messageHistory');

    var socket = new SockJS('/chat');
    stompClient = Stomp.over(socket);
    stompClient.connect();
    inputGroup.hide();

    $(document).on({
        ajaxStart: function () {
            body.addClass("loading");
        },
        ajaxStop: function () {
            body.removeClass("loading");
        }
    });

    $('.friend').click(function () {
        $(elemTableBody).text('');
        inputGroup.show();
        recipientId = $(this).attr('id');
        getMessageHistory(recipientId);
        if (subscribeTopic) {
            stompClient.unsubscribe(subscribeTopic.id);
        }
        subscribeTopic = stompClient.subscribe('/topic' + getChat(currentSenderId, recipientId), function (message) {
            var receivedMessage = JSON.parse(message.body);
            printMessageDetails(currentSenderId, receivedMessage.idFrom, receivedMessage.message, receivedMessage.date);
        });
    });

    $('#sendMessage').click(function () {
        var messageContentValue = $(messageContent).val();
        if (messageContentValue && stompClient) {
            var chatMessage = {
                idFrom: currentSenderId,
                idTo: recipientId,
                message: messageContentValue
            };
            stompClient.send('/app/chat' + getChat(currentSenderId, recipientId), {}, JSON.stringify(chatMessage));
            messageContent.val('');
        }
    });

    function getMessageHistory(recipientId) {
        $.ajax({
            method: 'GET',
            url: '/getMessageHistory?id=' + recipientId,
            dataType: 'json',
            success: function (data) {
                printMessageHistory(data);
            }
        });
    }

    function printMessageHistory(data) {
        for (var i = 0; i < data.length; i++) {
            var createdDate = data[i].localDate.replace(/[^-:0-:9]/gim, '  ');
            printMessageDetails(currentSenderId, data[i].accountFrom.id, data[i].message, createdDate);
        }
    }

    function printMessageDetails(currentSenderId, senderId, messageContentValue, createdDate) {
        if (currentSenderId == senderId) {
            $(elemTableBody).append(
                '<tr class="messageFrom">' +
                '<td class="alert alert-dark">' +
                messageContentValue + '<br><small class="textMutedFrom">' + createdDate +
                '</small>' +
                '</td>' +
                '<td></td>' +
                '</tr>'
            );
        } else {
            $(elemTableBody).append(
                '<tr class="messageTo">' +
                '<td></td>' +
                '<td class="alert alert-warning">' +
                messageContentValue + '<br><small class="textMutedTo">' + createdDate +
                '</small>' +
                '</td>' +
                '</tr>'
            );
        }
        scrollDownMessageHistory();
    }

    function scrollDownMessageHistory() {
        $(messageHistory).scrollTop(messageHistory[0].scrollHeight);
    }

    function getChat(currentSenderId, recipientId) {
        return currentSenderId < recipientId ? '/' + currentSenderId + '/' + recipientId : '/' + recipientId + '/' + currentSenderId;
    }
});
