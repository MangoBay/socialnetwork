$(function () {
    var search = $(".searchLabel").text();
    var body = $("body");
    var entityOnPage = 5;
    var firstPage = 1;

    $(body).append('<div class="wrapperNext next" id="nextAccount" ><svg width="18px" height="17px" viewBox="-1 0 18 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><polygon class="arrow" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon><polygon class="arrow-fixed" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon><path d="M-4.58892184e-16,0.56157424 L-4.58892184e-16,16.1929159 L9.708,8.33860465 L-1.64313008e-15,0.56157424 L-4.58892184e-16,0.56157424 Z M1.33333333,3.30246869 L7.62533333,8.34246869 L1.33333333,13.4327013 L1.33333333,3.30246869 L1.33333333,3.30246869 Z"></path></g></svg></div>');
    $(body).append('<div class="wrapperPrev prev" id="prevAccount"><svg width="18px" height="17px" viewBox="0 0 18 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="prev" transform="translate(8.500000, 8.500000) scale(-1, 1) translate(-8.500000, -8.500000)"><polygon class="arrow" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon><polygon class="arrow-fixed" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon><path d="M-1.48029737e-15,0.56157424 L-1.48029737e-15,16.1929159 L9.708,8.33860465 L-2.66453526e-15,0.56157424 L-1.48029737e-15,0.56157424 Z M1.33333333,3.30246869 L7.62533333,8.34246869 L1.33333333,13.4327013 L1.33333333,3.30246869 L1.33333333,3.30246869 Z"></path></g></svg></div>');
    $(body).append('<div class="wrapperNext next" id="nextGroup" ><svg width="18px" height="17px" viewBox="-1 0 18 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g><polygon class="arrow" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon><polygon class="arrow-fixed" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon><path d="M-4.58892184e-16,0.56157424 L-4.58892184e-16,16.1929159 L9.708,8.33860465 L-1.64313008e-15,0.56157424 L-4.58892184e-16,0.56157424 Z M1.33333333,3.30246869 L7.62533333,8.34246869 L1.33333333,13.4327013 L1.33333333,3.30246869 L1.33333333,3.30246869 Z"></path></g></svg></div>');
    $(body).append('<div class="wrapperPrev prev" id="prevGroup"><svg width="18px" height="17px" viewBox="0 0 18 17" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="prev" transform="translate(8.500000, 8.500000) scale(-1, 1) translate(-8.500000, -8.500000)"><polygon class="arrow" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon><polygon class="arrow-fixed" points="16.3746667 8.33860465 7.76133333 15.3067621 6.904 14.3175671 14.2906667 8.34246869 6.908 2.42790698 7.76 1.43613596"></polygon><path d="M-1.48029737e-15,0.56157424 L-1.48029737e-15,16.1929159 L9.708,8.33860465 L-2.66453526e-15,0.56157424 L-1.48029737e-15,0.56157424 Z M1.33333333,3.30246869 L7.62533333,8.34246869 L1.33333333,13.4327013 L1.33333333,3.30246869 L1.33333333,3.30246869 Z"></path></g></svg></div>');

    var currentPageAccount = 0;
    var countAccount = $(".countAccount").text();
    var countPageAccount = Math.ceil(countAccount / entityOnPage);
    var nextAccountSelector = $("#nextAccount");
    var prevAccountSelector = $("#prevAccount");
    var nextGroupSelector = $("#nextGroup");
    var prevGroupSelector = $("#prevGroup");

    $(prevGroupSelector).hide();
    $(nextGroupSelector).hide();
    $(nextAccountSelector).hide();
    $(prevAccountSelector).hide();

    $(".hrefToFirstAccountPage").click(function () {
        currentPageAccount = firstPage;
        sendAjaxAccountRequest(currentPageAccount);
        if (currentPageAccount <= firstPage) {
            hidePrevAccountAndPrevGroup();
            $(nextGroupSelector).hide();
            if (countAccount > entityOnPage) {
                $(nextAccountSelector).show();
            }
        } else {
            $(prevAccountSelector).show();
        }
    });

    $(nextAccountSelector).click(function () {
        currentPageAccount++;
        if (currentPageAccount > firstPage) {
            $(prevAccountSelector).show();
        }
        if (currentPageAccount === countPageAccount) {
            $(nextAccountSelector).hide();
            $(prevAccountSelector).show();
        }
        sendAjaxAccountRequest(currentPageAccount);
    });

    $(prevAccountSelector).click(function () {
        currentPageAccount--;
        if (currentPageAccount <= firstPage) {
            $(prevAccountSelector).hide();
            $(nextAccountSelector).show();
        }
        if (currentPageAccount > firstPage) {
            $(prevAccountSelector).show();
        }
        if (currentPageAccount < countPageAccount) {
            $(nextAccountSelector).show();
        }
        sendAjaxAccountRequest(currentPageAccount);
    });

    function hidePrevAccountAndPrevGroup() {
        $(prevAccountSelector).hide();
        $(prevGroupSelector).hide();
    }

    function sendAjaxAccountRequest(currentPage) {
        $.ajax({
            method: "GET",
            url: "/searchAccountsByPageAjax?search=" + search + "&page=" + currentPage,
            dataType: "json",
            success: function (data) {
                printAccounts(data);
            }
        });
    }

    function printAccounts(data) {
        var resultAccounts = $(".resultAccounts");
        $(resultAccounts).text("");
        $(".resultGroups").text("");
        var elemUl = $("<ul class='card-list'></ul>");
        $(resultAccounts).append(elemUl);
        for (var i = 0; i < data.length; i++) {
            var elemLi = $("<li class='card'></li>");
            var accountId = data[i].id;
            var accountCustomImage = '<img src="/accountPrintImage?id=' + accountId + '"/>';
            var accountDefaultImage = '<img src="/images/anonPerson.png"/>';
            var accountPhoto = data[i].photo;
            if (accountPhoto != null && accountPhoto.length !== 0) {
                $(elemLi).html('<a class="card-image is-loaded" href="/account?id=' + accountId + '" style="background-image: url(/accountPrintImage?id=' + accountId + ');" data-image-full="/accountPrintImage?id=' + accountId + '">' + accountCustomImage + '</a>');
            } else {
                $(elemLi).html('<a class="card-image is-loaded" href="/account?id=' + accountId + '" style="background-image: url(/images/anonPerson.png);" data-image-full="/images/anonPerson.png">' + accountDefaultImage + '</a>');
            }
            var descriptionCard = '<a class="card-description" href="/account?id=' + accountId + '"> <h2>' + data[i].firstName + '</h2> <p>' + data[i].lastName + '</p></a>';
            $(elemLi).append(descriptionCard);
            $(".card-list").append(elemLi);
        }
    }


// GROUPS
    var currentPageGroup = 0;
    var countGroup = $(".countGroup").text();
    var countPageGroup = Math.ceil(countGroup / entityOnPage);

    $(prevGroupSelector).hide();
    $(prevAccountSelector).hide();

    $(".hrefToFirstGroupPage").click(function () {
        currentPageGroup = firstPage;
        sendAjaxGroupRequest(currentPageGroup);
        if (currentPageGroup <= firstPage) {
            hidePrevAccountAndPrevGroup();
            $(nextAccountSelector).hide();
            if (countGroup > entityOnPage) {
                $(nextGroupSelector).show();
            }
        } else {
            $(prevGroupSelector).show();
        }
    });

    $(nextGroupSelector).click(function () {
        currentPageGroup++;
        if (currentPageGroup > firstPage) {
            $(prevGroupSelector).show();
        }
        if (currentPageGroup === countPageGroup) {
            $(nextGroupSelector).hide();
            $(prevGroupSelector).show();
        }
        sendAjaxGroupRequest(currentPageGroup);
    });

    $(prevGroupSelector).click(function () {
        currentPageGroup--;
        if (currentPageGroup <= firstPage) {
            $(prevGroupSelector).hide();
            $(nextGroupSelector).show();
        }
        if (currentPageGroup > firstPage) {
            $(prevGroupSelector).show();
        }
        if (currentPageGroup < countPageGroup) {
            $(nextGroupSelector).show();
        }
        sendAjaxGroupRequest(currentPageGroup);
    });

    function printGroups(data) {
        var resultGroups = $(".resultGroups");
        $(resultGroups).text("");
        $(".resultAccounts").text("");
        var elemUl = $("<ul class='card-list'></ul>");
        $(resultGroups).append(elemUl);
        for (var i = 0; i < data.length; i++) {
            var elemLi = $("<li class='card'></li>");
            var groupId = data[i].id;
            var groupCustomImage = '<img src="/groupPrintImage?groupId=' + groupId + '"/>';
            var groupDefaultImage = '<img src="/images/anonGroup.jpg"/>';
            var groupPhoto = data[i].photo;
            if (groupPhoto != null && groupPhoto.length !== 0) {
                $(elemLi).html('<a class="card-image is-loaded" href="/group?id=' + groupId + '" style="background-image: url(/groupPrintImage?groupId=' + groupId + ');" data-image-full="/groupPrintImage?groupId=' + groupId + '">' + groupCustomImage + '</a>');
            } else {
                $(elemLi).html('<a class="card-image is-loaded" href="/group?id=' + groupId + '" style="background-image: url(/images/anonGroup.jpg);" data-image-full="/images/anonGroup.png">' + groupDefaultImage + '</a>');
            }
            var descriptionCard = '<a class="card-description" href="/group?id=' + groupId + '"> <h2>' + data[i].name + '</h2></a>';
            $(elemLi).append(descriptionCard);
            $(".card-list").append(elemLi);
        }
    }

    function sendAjaxGroupRequest(currentPage) {
        $.ajax({
            method: "GET",
            url: "/searchGroupsByPageAjax?search=" + search + "&page=" + currentPage,
            dataType: "json",
            success: function (data) {
                printGroups(data);
            }
        });
    }
});
