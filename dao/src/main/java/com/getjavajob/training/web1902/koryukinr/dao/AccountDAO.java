package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.Account;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountDAO extends CrudRepository<Account, Integer> {

    Account findByEmailAndPassword(String email, String password);

    Account findByEmail(String email);

    Account findById(int id);

    List<Account> findAll();

}
