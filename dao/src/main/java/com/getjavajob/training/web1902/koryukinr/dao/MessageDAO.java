package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.Account;
import com.getjavajob.training.web1902.koryukinr.common.Message;
import com.getjavajob.training.web1902.koryukinr.common.MessageType;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MessageDAO extends CrudRepository<Message, Integer> {

    Message findById(int id);

    List<Message> findAllByMessageTypeAndIdToOrderByLocalDateDesc(@Param("messageType") MessageType messageType,
                                                                  @Param("idTo") int idTo);

    @Query("select m from Message m where (m.accountFrom = :accountFrom and m.idTo = :idTwo and m.messageType = 'PRIVATE') or " +
            "(m.accountFrom = :idTwo and m.idTo = :accountFrom and m.messageType = 'PRIVATE') order by m.localDate asc")
    List<Message> findAllMessagesBetweenAccountFromAndIdTwoByMessageType(@Param("accountFrom") Account accountFrom,
                                                                         @Param("idTwo") int idTwo);

    void deleteAllByAccountFromOrIdTo(Account account, int idTo);

}
