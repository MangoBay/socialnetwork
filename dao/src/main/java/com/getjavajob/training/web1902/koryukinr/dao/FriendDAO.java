package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.Friendship;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FriendDAO extends CrudRepository<Friendship, Integer> {

    Friendship findById(int id);

    Friendship findByActionId(int actionId);

    Friendship findFriendshipByIdFromAndIdTo(int idFrom, int idTo);

    List<Friendship> findAll();

    List<Friendship> findAllByIdFromOrIdTo(int idFrom, int idTo);

    void delete(Friendship friendship);

}
