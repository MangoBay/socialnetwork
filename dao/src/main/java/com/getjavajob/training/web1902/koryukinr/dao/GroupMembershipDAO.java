package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.GroupMembership;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupMembershipDAO extends CrudRepository<GroupMembership, Integer> {

    GroupMembership findById(int id);

    GroupMembership findByAccountIdAndGroupId(int accountId, int groupId);

    List<GroupMembership> findAll();

    List<GroupMembership> findAllByGroupId(int groupId);

    void deleteByAccountIdAndGroupId(int accountId, int groupId);

}
