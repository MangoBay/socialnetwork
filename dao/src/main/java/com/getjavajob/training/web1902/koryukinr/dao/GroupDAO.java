package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface GroupDAO extends CrudRepository<Group, Integer> {

    Group findById(int id);

    Group findByName(String name);

    List<Group> findAll();

    List<Group> findAllByOwner(int owner);

}