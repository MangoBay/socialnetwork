package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.GroupMembership;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class GroupMembershipDAOTest {
    @Autowired
    private GroupMembershipDAO groupMembershipDAO;
    private GroupMembership groupMembership;

    @Before
    public void init() {
        groupMembership = new GroupMembership(3, 3,false, false);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void save() {
        GroupMembership expected = groupMembershipDAO.save(groupMembership);
        GroupMembership actual = groupMembershipDAO.findById(1);

        assertEquals(expected, actual);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void delete() {
        groupMembershipDAO.save(groupMembership);
        groupMembershipDAO.deleteByAccountIdAndGroupId(3, 3);

        assertEquals(0, groupMembershipDAO.findAll().size());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void getByAccountAndGroup() {
        groupMembershipDAO.save(groupMembership);
        GroupMembership groupMembershipLocal = groupMembershipDAO.findByAccountIdAndGroupId(3, 3);

        assertEquals(groupMembershipLocal, groupMembership);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void findAll() {
        assertEquals(0, groupMembershipDAO.findAll().size());
    }
}
