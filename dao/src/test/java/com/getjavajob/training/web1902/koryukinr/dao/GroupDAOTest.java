package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.Group;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class GroupDAOTest {
    @Autowired
    private GroupDAO groupDAO;
    private Group group;

    @Before
    public void init() {
        group = new Group("Sport", 2, "This sport", LocalDate.now());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void insert() {
        Group expected = groupDAO.save(group);
        Group actual = groupDAO.findById(4);

        assertEquals(expected, actual);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void update() {
        groupDAO.save(group);

        group.setName("Dance");
        Group expected = groupDAO.save(group);
        Group actual = groupDAO.findById(4);

        assertEquals(expected, actual);
    }

    @Test
    @Sql("classpath:create.sql")
    public void get() {
        assertEquals("Programming", groupDAO.findById(3).getName());
    }

    @Test
    @Sql("classpath:create.sql")
    public void findAll() {
        List<Group> groups = groupDAO.findAll();

        assertEquals(3, groups.size());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void delete() {
        groupDAO.deleteById(1);

        assertEquals(2, groupDAO.findAll().size());
    }

    @Test
    @Sql("classpath:create.sql")
    public void findByName() {
        assertEquals("Here art", groupDAO.findByName("Art").getDescription());
    }

    @Test
    @Sql("classpath:create.sql")
    public void findAllByOwner() {
        assertEquals(2, groupDAO.findAllByOwner(2).size());
    }
}
