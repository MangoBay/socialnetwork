package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class AccountDAOTest {
    @Autowired
    private AccountDAO accountDAO;
    @Autowired
    private MessageDAO messageDAO;
    private Account account;

    @Before
    public void init() {
        account = new Account("Михаил", "Петров", "misha@mail.ru", "mishka");
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void insert() {
        Account expected = accountDAO.save(account);
        Account actual = accountDAO.findById(4);

        assertEquals(expected, actual);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void update() {
        accountDAO.save(account);
        account.setFirstName("Александр");
        Account expected = accountDAO.save(account);
        Account actual = accountDAO.findById(4);

        assertEquals(expected, actual);
    }

    @Test
    @Sql("classpath:create.sql")
    public void get() {
        Account expected = accountDAO.save(account);
        Account actual = accountDAO.findById(4);

        assertEquals(expected.getFirstName(), actual.getFirstName());
    }

    @Test
    @Sql("classpath:create.sql")
    public void find() {
        Account expected = accountDAO.findByEmailAndPassword("ivan@mail.ru", "ivan123");
        Account actual = accountDAO.findById(1);

        assertEquals(expected.getFirstName(), actual.getFirstName());
    }

    @Test
    @Sql("classpath:create.sql")
    public void findAll() {
        int expectedSizeAccounts = accountDAO.findAll().size();

        assertEquals(3, expectedSizeAccounts);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void delete() {
        messageDAO.deleteAllByAccountFromOrIdTo(accountDAO.findById(1), 1);
        accountDAO.deleteById(1);
        List<Account> accounts = accountDAO.findAll();

        assertEquals(2, accounts.size());
    }
}




