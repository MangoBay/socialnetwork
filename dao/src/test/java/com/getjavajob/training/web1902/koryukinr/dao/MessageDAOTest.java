package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.Account;
import com.getjavajob.training.web1902.koryukinr.common.Message;
import com.getjavajob.training.web1902.koryukinr.common.MessageType;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class MessageDAOTest {

    @Autowired
    private MessageDAO messageDAO;
    @Autowired
    private AccountDAO accountDAO;

    private Message message;
    private Account account;

    @Before
    public void init() {
        account = new Account("1", "2", "3", "4");
        message = new Message(account, 2, "Hello, Amir");
        message.setLocalDate(LocalDateTime.now());
        message.setMessageType(MessageType.PRIVATE);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void insert() {
        accountDAO.save(account);
        messageDAO.save(message);

        assertEquals("Hello, Amir", messageDAO.findById(6).getMessage());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void getMessagesBetweenTwoAccounts() {
        Account account = accountDAO.findById(3);
        List<Message> messages = messageDAO.findAllMessagesBetweenAccountFromAndIdTwoByMessageType(account, 1);

        assertEquals(3, messages.size());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void findAll() {
        List<Message> messages = messageDAO.findAllByMessageTypeAndIdToOrderByLocalDateDesc(MessageType.GROUP_WALL, 3);

        assertEquals(1, messages.size());
    }
}
