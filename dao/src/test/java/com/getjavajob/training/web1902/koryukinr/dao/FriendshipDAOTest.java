package com.getjavajob.training.web1902.koryukinr.dao;

import com.getjavajob.training.web1902.koryukinr.common.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-test-context.xml"})
public class FriendshipDAOTest {
    @Autowired
    private FriendDAO friendDAO;
    @Autowired
    private AccountDAO accountDAO;

    private Account account1;
    private Account account2;

    private Friendship friend1;
    private Friendship friend2;
    private Friendship friend3;

    @Transactional
    @Before
    public void init() {
        LocalDate date = LocalDate.now();
        account1 = new Account("Roman", "Koryukin", "koryukin@mail.ru",
                "123", date, date);
        account2 = new Account("Sasha", "Svetlakova", "sasha@mail.ru",
                "222", date, date);
        Account account3 = new Account("Misha", "Smirnov", "olegovna@mail.ru",
                "444", date, date);
        Account account4 = new Account("Sveta", "Besedina", "beseda@mail.ru",
                "555", date, date);

        accountDAO.save(account1);
        accountDAO.save(account2);
        accountDAO.save(account3);
        accountDAO.save(account4);

        friend1 = new Friendship(account1.getId(), account2.getId(), Status.PENDING, account1.getId());
        friend2 = new Friendship(account1.getId(), account4.getId(), Status.PENDING, account1.getId());
        friend3 = new Friendship(account1.getId(), account3.getId(), Status.PENDING, account3.getId());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void insert() {
        Friendship expected = friendDAO.save(friend1);
        Friendship actual = friendDAO.findById(1);

        assertEquals(expected, actual);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void update() {
        friendDAO.save(friend1);
        friend1.setActionId(account2.getId());
        friend1.setStatus(Status.ACCEPTED);
        friend1.setIdFrom(account2.getId());
        friend1.setIdTo(account1.getId());
        friendDAO.save(friend1);

        assertEquals(5, friendDAO.findByActionId(account2.getId()).getIdFrom());
        assertEquals(4, friendDAO.findByActionId(account2.getId()).getIdTo());
        assertEquals(Status.ACCEPTED, friendDAO.findByActionId(account2.getId()).getStatus());
        assertEquals(5, friendDAO.findByActionId(account2.getId()).getActionId());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void deleteById() {
        friendDAO.save(friend1);
        friendDAO.deleteById(friend1.getId());

        assertEquals(0, friendDAO.findAll().size());
    }


    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void findById() {
        friendDAO.save(friend1);

        assertEquals("PENDING", friendDAO.findById(1).getStatus().toString());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void findAll() {
        friendDAO.save(friend2);
        friendDAO.save(friend2);
        List<Friendship> friends = friendDAO.findAll();

        assertEquals(1, friends.size());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void getAllFriends() {
        friendDAO.save(friend1);
        friendDAO.save(friend2);
        friendDAO.save(friend3);

        List<Friendship> friends = friendDAO.findAllByIdFromOrIdTo(account1.getId(), account1.getId());
        assertEquals(3, friends.size());
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void findFriendshipByIdFromAndIdTo() {
        friendDAO.save(friend1);
        Friendship friendship = friendDAO.findFriendshipByIdFromAndIdTo(account1.getId(), account2.getId());
        
        assertEquals(friend1, friendship);
    }

    @Transactional
    @Test
    @Sql("classpath:create.sql")
    public void deleteAllFriendship() {
        friendDAO.save(friend1);
        friendDAO.delete(friend1);

        assertEquals(0, friendDAO.findAll().size());
    }
}
