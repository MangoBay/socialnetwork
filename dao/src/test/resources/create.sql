DROP TABLE IF EXISTS friendship;
DROP TABLE IF EXISTS phone;
DROP TABLE IF EXISTS grp;
DROP TABLE IF EXISTS account;
DROP TABLE IF EXISTS group_membership;
DROP TABLE IF EXISTS message;

CREATE TABLE account (
  id                     INT(11)      NOT NULL AUTO_INCREMENT,
  first_name             VARCHAR(45)  NOT NULL,
  middle_name            VARCHAR(45)  NULL     DEFAULT NULL,
  last_name              VARCHAR(45)  NOT NULL,
  birth_date             DATE                  DEFAULT CURRENT_DATE,
  registration_date      DATE                  DEFAULT CURRENT_DATE,
  home_address           VARCHAR(45)  NULL     DEFAULT NULL,
  work_address           VARCHAR(45)  NULL     DEFAULT NULL,
  email                  VARCHAR(45)  NOT NULL UNIQUE,
  password               VARCHAR(45)  NOT NULL,
  skype                  VARCHAR(45)  NULL     DEFAULT NULL,
  additional_information VARCHAR(200) NULL     DEFAULT NULL,
  photo                  LONGBLOB     NULL     DEFAULT NULL,
  is_admin               TINYINT(1)   NOT NULL DEFAULT '0',
  PRIMARY KEY (id)
);

CREATE TABLE friendship (
  id              INT(11)     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  account_id_from INT(11)     NOT NULL,
  account_id_to   INT(11)     NOT NULL,
  status          VARCHAR(45) NOT NULL,
  action_user_id  INT(11)     NOT NULL,
  FOREIGN KEY (action_user_id) REFERENCES account (id)
);

CREATE TABLE grp (
  id           INT(11)     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  name         VARCHAR(45) NOT NULL,
  description  VARCHAR(45) NOT NULL,
  created_date DATE                 DEFAULT CURRENT_DATE,
  photo        LONGBLOB    NULL     DEFAULT NULL,
  owner        INT(11)     NOT NULL,
  FOREIGN KEY (owner) REFERENCES account (id)

);

CREATE TABLE phone (
  id         INT(11)     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  number     VARCHAR(45) NOT NULL,
  account_id INT(11)     NOT NULL,
  FOREIGN KEY (account_id) REFERENCES account (id)
);

CREATE TABLE IF NOT EXISTS group_membership (
  id         INT(11)    NOT NULL AUTO_INCREMENT,
  account_id INT(11)    NOT NULL,
  group_id   INT(11)    NOT NULL,
  is_admin   TINYINT(1) NULL     DEFAULT '0',
  is_accept  TINYINT(1) NULL     DEFAULT '0',
  FOREIGN KEY (account_id) REFERENCES account (id),
  FOREIGN KEY (group_id) REFERENCES grp (id),
  PRIMARY KEY (id)
);

CREATE TABLE message (
  id           INT(11)     NOT NULL AUTO_INCREMENT PRIMARY KEY,
  id_from      INT(11)     NOT NULL,
  id_to        INT(11)     NOT NULL,
  message      TEXT        NOT NULL,
  local_date   TIMESTAMP            DEFAULT CURRENT_DATE,
  message_type VARCHAR(45) NOT NULL,
  FOREIGN KEY (id_from) REFERENCES account (id),
);

INSERT INTO account (first_name, last_name, home_address, email, password)
VALUES ('Иван', 'Григорьевский', 'SPB', 'ivan@mail.ru', 'ivan123');

INSERT INTO account (first_name, last_name, home_address, email, password)
VALUES ('Амир', 'Мунипов', 'SPB', 'amir@mail.ru', 'amirM');

INSERT INTO account (first_name, last_name, home_address, email, password)
VALUES ('Роман', 'Корюкин', 'SPB', 'roman@mail.ru', 'romka');

INSERT INTO grp (id, name, owner, description) VALUES (1, 'Music', 2, 'Here music');
INSERT INTO grp (id, name, owner, description) VALUES (2, 'Art', 2, 'Here art');
INSERT INTO grp (id, Name, owner, description) VALUES (3, 'Programming', 3, 'Here programming');

INSERT INTO phone (id, number, account_id) VALUES (1, '89123221286', 3);
INSERT INTO phone (id, number, account_id) VALUES (2, '493-231', 3);
INSERT INTO phone (id, number, account_id) VALUES (3, '912', 3);
INSERT INTO phone (id, number, account_id) VALUES (4, '89123241821', 2);
INSERT INTO phone (id, number, account_id) VALUES (5, '91232412341', 2);

INSERT INTO message (id, id_from, id_to, message, message_type)
VALUES (1, 1, 3, 'Hello, Roman. Accept me request', 'ACCOUNT_WALL');
INSERT INTO message (id, id_from, id_to, message, message_type)
VALUES (2, 1, 3, 'Hello, Roman. How are you?', 'PRIVATE');
INSERT INTO message (id, id_from, id_to, message, message_type)
VALUES (3, 1, 3, 'Hello, Roman. Why are you ignoring me?', 'PRIVATE');
INSERT INTO message (id, id_from, id_to, message, message_type)
VALUES (4, 3, 1, 'Hello, Ivan. How are you?', 'PRIVATE');

INSERT INTO message (id, id_from, id_to, message, message_type)
VALUES (5, 1, 3, 'How to use lambda?', 'GROUP_WALL');
