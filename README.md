## Social Network  
**Functionality:**  

- Registration  
- Authentication  
- Account page display  
- Edit account  
- Friendship  
- Membership  
- Search for accounts and groups  
- Autocomplete (AJAX)  
- Pagination (AJAX)  
- Export/Import account to/from xml  
- Upload avatar  


  - *Social Network Roles:*  
    - User (Private access)  
    - Admin (General access)  
  
  - *Groups:*  
    - Creating/Deleting/Editing groups  
    - Sending applications to groups  
    - Upload logo  
    - Receiving and confirming applications to groups  
      - *Roles:*  
        - Admin  
        - Member  
        - Guest  
    
  - *Messages:*  
    - Private messages (WebSocket)  
    - Messages on the wall of the group  
    - Messages on the wall of the account  
 
**Tools:**  
JDK 8, Spring 5 (Boot, Data, Security, Web), XStream 1.4, JPA 2/Hibernate 5, jQuery 3, Twitter Bootstrap 4.3, 
JUnit 4, Mockito, Maven 3, Git/Bitbucket, Tomcat 8, MySQL 5.7/H2 1.4, Heroku Cloud, IntelliJIDEA 2019.2.  

**Screenshots:**
  
___  

![login](screenshots/login.jpg)  
  
___  

![register](screenshots/register.jpg)
  
___  
  
![account](screenshots/account.jpg)
  
___  
  
![group](screenshots/group.jpg)
  
___  

![messages](screenshots/messages.jpg)
  
___   
  
![search](screenshots/search.jpg)
  
___  
  

**App details:**
> **The app is fully responsive**  
>
> Heroku app link: https://socnet-koryukinr.herokuapp.com/  
> Demo login: demo@mail.ru  
> Demo pass: demo  

_  
**Koryukin Roman**  
Training getJavaJob  
http://www.getjavajob.com
